﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleTest : MonoBehaviour
{
    public GameObject cube;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public void Update()
    {
        
    }

    public void scalingcube()
    {
        cube.gameObject.transform.localScale = new Vector3(10, 10, 10);
    }

    public void stopscalingcube()
    {
        cube.gameObject.transform.localScale = new Vector3(1, 1, 1);
    }
}
