﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Highlighter : MonoBehaviour
{
    private Material standard;
    private Material highlighted;

    [SerializeField] private float outlineSize = 0.001f;
    [SerializeField] private Color highLightColor = Color.green;
    [SerializeField] private List<GameObject> objectsToIgnore;
    public bool isGrabbed;
    
    private Renderer renderer;

    void Start()
    {
        isGrabbed = false;
        renderer = GetComponent<Renderer>();
        standard = renderer.material;

        highlighted = new Material(renderer.material);
        highlighted.shader = Shader.Find("Custom/Highlight");
        highlighted.SetFloat("_Outline", outlineSize);
        highlighted.SetColor("_OutlineColor", highLightColor);
    }

    private void OnTriggerStay(Collider other)
    {
        bool canIgnore = false;

        foreach (GameObject obj in objectsToIgnore)
        {
            if (other.gameObject.Equals(obj))
            {
                canIgnore = true;
            }
        }

        if (!canIgnore)
        {
            if (isGrabbed)
            {
                this.StopHighlight();
            }
            else {
                this.StartHighlight();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        bool canIgnore = false;

        foreach (GameObject obj in objectsToIgnore)
        {
            if (other.gameObject.Equals(obj))
            {
                canIgnore = true;
            }
        }

        if (!canIgnore)
        {
            this.StopHighlight();
        }
    }
    
    public void StartHighlight()
    {
        renderer.material = highlighted;
    }
    public void StopHighlight()
    {
        renderer.material = standard;
    }
}
