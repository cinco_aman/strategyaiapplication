﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleSubPanel : MonoBehaviour
{
    public GameObject LiveLonger;
    public GameObject LiveHappier;
    public GameObject LiveHealthier;
    public GameObject Connect_with_your_customers;
    public GameObject Improve_Effciency_And_Roi;
    public GameObject Get_More_Time_To_Do_What_Inspires_You;
    public GameObject Saving_The_Planet;
    public GameObject Improving_Communication;
    public GameObject Increasing_Food_Access_For_All;
    public GameObject InSports;
    public GameObject In_Your_Home;
    public GameObject In_Entertainment;
    public GameObject AIForYouQuestion1;
    public GameObject AIForYouQuestion2;
    public GameObject AIForYouQuestion3;
    public GameObject AIForYourBusinessQuestion1;
    public GameObject AIForYourBusinessQuestion2;
    public GameObject AIForYourBusinessQuestion3;
    public GameObject AIForGoodQuestion1;
    public GameObject AIForGoodQuestion2;
    public GameObject AIForGoodQuestion3;
    public GameObject AIAroundYouQuestion1;
    public GameObject AIAroundYouQuestion2;
    public GameObject AIAroundYouQuestion3;
    public GameObject AIForYou;
    public GameObject AIForYourBusiness;
    public GameObject AIForGood;
    public GameObject AIAroundYou;

    public TwoHandGestureController Gesture;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ThingsHappenWhenLiveLongerIsSelected()
    {
        AIForYouQuestion1.SetActive(true);
        if (AIForYouQuestion1.activeSelf)
        {
            AIForYouQuestion2.SetActive(false);
            AIForYouQuestion3.SetActive(false);
            AIForYourBusinessQuestion1.SetActive(false);
            AIForYourBusinessQuestion2.SetActive(false);
            AIForYourBusinessQuestion3.SetActive(false);
            AIForGoodQuestion1.SetActive(false);
            AIForGoodQuestion2.SetActive(false);
            AIForGoodQuestion3.SetActive(false);
            AIAroundYouQuestion1.SetActive(false);
            AIAroundYouQuestion2.SetActive(false);
            AIAroundYouQuestion3.SetActive(false);
            AIForYou.SetActive(false);
            AIForYourBusiness.SetActive(false);
            AIForGood.SetActive(false);
            AIAroundYou.SetActive(false);

            Gesture.GestureForMainMenuAIForYou.SetActive(false);
            Gesture.GestureForMainMenuAIForYourBusiness.SetActive(false);
            Gesture.GestureForMainMenuAIForGood.SetActive(false);
            Gesture.GestureForMainMenuAIAroundYou.SetActive(false);
        }
    }
    
    public void ThingsHappenWhenLiveHappierIsSelected()
    {
        AIForYouQuestion2.SetActive(true);
        if (AIForYouQuestion2.activeSelf)
        {
            AIForYouQuestion1.SetActive(false);
            AIForYouQuestion3.SetActive(false);
            AIForYourBusinessQuestion1.SetActive(false);
            AIForYourBusinessQuestion2.SetActive(false);
            AIForYourBusinessQuestion3.SetActive(false);
            AIForGoodQuestion1.SetActive(false);
            AIForGoodQuestion2.SetActive(false);
            AIForGoodQuestion3.SetActive(false);
            AIAroundYouQuestion1.SetActive(false);
            AIAroundYouQuestion2.SetActive(false);
            AIAroundYouQuestion3.SetActive(false);
            AIForYou.SetActive(false);
            AIForYourBusiness.SetActive(false);
            AIForGood.SetActive(false);
            AIAroundYou.SetActive(false);

            Gesture.GestureForMainMenuAIForYou.SetActive(false);
            Gesture.GestureForMainMenuAIForYourBusiness.SetActive(false);
            Gesture.GestureForMainMenuAIForGood.SetActive(false);
            Gesture.GestureForMainMenuAIAroundYou.SetActive(false);


        }
    }

    public void ThingsHappenWhenLiveHealthierIsSelected()
    {
        AIForYouQuestion3.SetActive(true);
        if (AIForYouQuestion3.activeSelf)
        {
            AIForYouQuestion1.SetActive(false);
            AIForYouQuestion2.SetActive(false);
            AIForYourBusinessQuestion1.SetActive(false);
            AIForYourBusinessQuestion2.SetActive(false);
            AIForYourBusinessQuestion3.SetActive(false);
            AIForGoodQuestion1.SetActive(false);
            AIForGoodQuestion2.SetActive(false);
            AIForGoodQuestion3.SetActive(false);
            AIAroundYouQuestion1.SetActive(false);
            AIAroundYouQuestion2.SetActive(false);
            AIAroundYouQuestion3.SetActive(false);
            AIForYou.SetActive(false);
            AIForYourBusiness.SetActive(false);
            AIForGood.SetActive(false);
            AIAroundYou.SetActive(false);

            Gesture.GestureForMainMenuAIForYou.SetActive(false);
            Gesture.GestureForMainMenuAIForYourBusiness.SetActive(false);
            Gesture.GestureForMainMenuAIForGood.SetActive(false);
            Gesture.GestureForMainMenuAIAroundYou.SetActive(false);




        }

    }

    public void ThingsHappenWhenConnectWithYourCustomerIsSelected()
    {
        AIForYourBusinessQuestion1.SetActive(true);
        if (AIForYourBusinessQuestion1.activeSelf)
        {
            AIForYouQuestion1.SetActive(false);
            AIForYouQuestion2.SetActive(false);
            AIForYouQuestion3.SetActive(false);
            AIForYourBusinessQuestion2.SetActive(false);
            AIForYourBusinessQuestion3.SetActive(false);
            AIForGoodQuestion1.SetActive(false);
            AIForGoodQuestion2.SetActive(false);
            AIForGoodQuestion3.SetActive(false);
            AIAroundYouQuestion1.SetActive(false);
            AIAroundYouQuestion2.SetActive(false);
            AIAroundYouQuestion3.SetActive(false);
            AIForYou.SetActive(false);
            AIForYourBusiness.SetActive(false);
            AIForGood.SetActive(false);
            AIAroundYou.SetActive(false);

            Gesture.GestureForMainMenuAIForYou.SetActive(false);
            Gesture.GestureForMainMenuAIForYourBusiness.SetActive(false);
            Gesture.GestureForMainMenuAIForGood.SetActive(false);
            Gesture.GestureForMainMenuAIAroundYou.SetActive(false);

        }
    }

    public void ThingsHappenWhenImproveEfficiencyAndROIIsSelected()
    {

        AIForYourBusinessQuestion2.SetActive(true);
        if (AIForYourBusinessQuestion2.activeSelf)
        {
            AIForYouQuestion1.SetActive(false);
            AIForYouQuestion2.SetActive(false);
            AIForYouQuestion3.SetActive(false);
            AIForYourBusinessQuestion1.SetActive(false);
            AIForYourBusinessQuestion3.SetActive(false);
            AIForGoodQuestion1.SetActive(false);
            AIForGoodQuestion2.SetActive(false);
            AIForGoodQuestion3.SetActive(false);
            AIAroundYouQuestion1.SetActive(false);
            AIAroundYouQuestion2.SetActive(false);
            AIAroundYouQuestion3.SetActive(false);
            AIForYou.SetActive(false);
            AIForYourBusiness.SetActive(false);
            AIForGood.SetActive(false);
            AIAroundYou.SetActive(false);

            Gesture.GestureForMainMenuAIForYou.SetActive(false);
            Gesture.GestureForMainMenuAIForYourBusiness.SetActive(false);
            Gesture.GestureForMainMenuAIForGood.SetActive(false);
            Gesture.GestureForMainMenuAIAroundYou.SetActive(false);

        }

    }

    public void ThingsHappenWhenGetMoreTimeToDoWhatInspiresYouIsSelected()
    {
        AIForYourBusinessQuestion3.SetActive(true);
        if (AIForYourBusinessQuestion3.activeSelf)
        {
            AIForYouQuestion1.SetActive(false);
            AIForYouQuestion2.SetActive(false);
            AIForYouQuestion3.SetActive(false);
            AIForYourBusinessQuestion1.SetActive(false);
            AIForYourBusinessQuestion2.SetActive(false);
            AIForGoodQuestion1.SetActive(false);
            AIForGoodQuestion2.SetActive(false);
            AIForGoodQuestion3.SetActive(false);
            AIAroundYouQuestion1.SetActive(false);
            AIAroundYouQuestion2.SetActive(false);
            AIAroundYouQuestion3.SetActive(false);
            AIForYou.SetActive(false);
            AIForYourBusiness.SetActive(false);
            AIForGood.SetActive(false);
            AIAroundYou.SetActive(false);

            Gesture.GestureForMainMenuAIForYou.SetActive(false);
            Gesture.GestureForMainMenuAIForYourBusiness.SetActive(false);
            Gesture.GestureForMainMenuAIForGood.SetActive(false);
            Gesture.GestureForMainMenuAIAroundYou.SetActive(false);


        }
    }

    public void ThingsHappenWhenSavingThePlanetIsSelected()
    {
        AIForGoodQuestion1.SetActive(true);
        if (AIForGoodQuestion1.activeSelf)
        {
            AIForYouQuestion1.SetActive(false);
            AIForYouQuestion2.SetActive(false);
            AIForYouQuestion3.SetActive(false);
            AIForYourBusinessQuestion1.SetActive(false);
            AIForYourBusinessQuestion2.SetActive(false);
            AIForYourBusinessQuestion3.SetActive(false);
            AIForGoodQuestion2.SetActive(false);
            AIForGoodQuestion3.SetActive(false);
            AIAroundYouQuestion1.SetActive(false);
            AIAroundYouQuestion2.SetActive(false);
            AIAroundYouQuestion3.SetActive(false);
            AIForYou.SetActive(false);
            AIForYourBusiness.SetActive(false);
            AIForGood.SetActive(false);
            AIAroundYou.SetActive(false);

            Gesture.GestureForMainMenuAIForYou.SetActive(false);
            Gesture.GestureForMainMenuAIForYourBusiness.SetActive(false);
            Gesture.GestureForMainMenuAIForGood.SetActive(false);
            Gesture.GestureForMainMenuAIAroundYou.SetActive(false);


        }
    }

    public void ThingsHappenWhenImprovingCommunicationIsSelected()
    {
        AIForGoodQuestion2.SetActive(true);
        if (AIForGoodQuestion2.activeSelf)
        {
            AIForYouQuestion1.SetActive(false);
            AIForYouQuestion2.SetActive(false);
            AIForYouQuestion3.SetActive(false);
            AIForYourBusinessQuestion1.SetActive(false);
            AIForYourBusinessQuestion2.SetActive(false);
            AIForYourBusinessQuestion3.SetActive(false);
            AIForGoodQuestion1.SetActive(false);
            AIForGoodQuestion3.SetActive(false);
            AIAroundYouQuestion1.SetActive(false);
            AIAroundYouQuestion2.SetActive(false);
            AIAroundYouQuestion3.SetActive(false);
            AIForYou.SetActive(false);
            AIForYourBusiness.SetActive(false);
            AIForGood.SetActive(false);
            AIAroundYou.SetActive(false);

            Gesture.GestureForMainMenuAIForYou.SetActive(false);
            Gesture.GestureForMainMenuAIForYourBusiness.SetActive(false);
            Gesture.GestureForMainMenuAIForGood.SetActive(false);
            Gesture.GestureForMainMenuAIAroundYou.SetActive(false);
        }
    }

    public void ThingsHappenWhenIncreasingFoodAccessForAllIsSelected()
    {

        AIForGoodQuestion3.SetActive(true);
        if (AIForGoodQuestion3.activeSelf)
        {
            AIForYouQuestion1.SetActive(false);
            AIForYouQuestion2.SetActive(false);
            AIForYouQuestion3.SetActive(false);
            AIForYourBusinessQuestion1.SetActive(false);
            AIForYourBusinessQuestion2.SetActive(false);
            AIForYourBusinessQuestion3.SetActive(false);
            AIForGoodQuestion1.SetActive(false);
            AIForGoodQuestion2.SetActive(false);
            AIAroundYouQuestion1.SetActive(false);
            AIAroundYouQuestion2.SetActive(false);
            AIAroundYouQuestion3.SetActive(false);
            AIForYou.SetActive(false);
            AIForYourBusiness.SetActive(false);
            AIForGood.SetActive(false);
            AIAroundYou.SetActive(false);

            Gesture.GestureForMainMenuAIForYou.SetActive(false);
            Gesture.GestureForMainMenuAIForYourBusiness.SetActive(false);
            Gesture.GestureForMainMenuAIForGood.SetActive(false);
            Gesture.GestureForMainMenuAIAroundYou.SetActive(false);
        }

    }

    public void ThingsHappenWhenInSportsIsSelected()
    {
        AIAroundYouQuestion1.SetActive(true);
        if (AIAroundYouQuestion1.activeSelf)
        {
            AIForYouQuestion1.SetActive(false);
            AIForYouQuestion2.SetActive(false);
            AIForYouQuestion3.SetActive(false);
            AIForYourBusinessQuestion1.SetActive(false);
            AIForYourBusinessQuestion2.SetActive(false);
            AIForYourBusinessQuestion3.SetActive(false);
            AIForGoodQuestion1.SetActive(false);
            AIForGoodQuestion2.SetActive(false);
            AIForGoodQuestion3.SetActive(false);
            AIAroundYouQuestion2.SetActive(false);
            AIAroundYouQuestion3.SetActive(false);
            AIForYou.SetActive(false);
            AIForYourBusiness.SetActive(false);
            AIForGood.SetActive(false);
            AIAroundYou.SetActive(false);

            Gesture.GestureForMainMenuAIForYou.SetActive(false);
            Gesture.GestureForMainMenuAIForYourBusiness.SetActive(false);
            Gesture.GestureForMainMenuAIForGood.SetActive(false);
            Gesture.GestureForMainMenuAIAroundYou.SetActive(false);
        }
    }

    public void ThingsHappenWhenInYourHomeIsSelected()
    {
        AIAroundYouQuestion2.SetActive(true);
        if (AIAroundYouQuestion2.activeSelf)
        {
            AIForYouQuestion1.SetActive(false);
            AIForYouQuestion2.SetActive(false);
            AIForYouQuestion3.SetActive(false);
            AIForYourBusinessQuestion1.SetActive(false);
            AIForYourBusinessQuestion2.SetActive(false);
            AIForYourBusinessQuestion3.SetActive(false);
            AIForGoodQuestion1.SetActive(false);
            AIForGoodQuestion2.SetActive(false);
            AIForGoodQuestion3.SetActive(false);
            AIAroundYouQuestion1.SetActive(false);
            AIAroundYouQuestion3.SetActive(false);
            AIForYou.SetActive(false);
            AIForYourBusiness.SetActive(false);
            AIForGood.SetActive(false);
            AIAroundYou.SetActive(false);

            Gesture.GestureForMainMenuAIForYou.SetActive(false);
            Gesture.GestureForMainMenuAIForYourBusiness.SetActive(false);
            Gesture.GestureForMainMenuAIForGood.SetActive(false);
            Gesture.GestureForMainMenuAIAroundYou.SetActive(false);

        }
    }

    public void ThingsHappenWhenInEntertainmentIsSelected()
    {
        AIAroundYouQuestion3.SetActive(true);
        if (AIAroundYouQuestion3.activeSelf)
        {
            AIForYouQuestion1.SetActive(false);
            AIForYouQuestion2.SetActive(false);
            AIForYouQuestion3.SetActive(false);
            AIForYourBusinessQuestion1.SetActive(false);
            AIForYourBusinessQuestion2.SetActive(false);
            AIForYourBusinessQuestion3.SetActive(false);
            AIForGoodQuestion1.SetActive(false);
            AIForGoodQuestion2.SetActive(false);
            AIForGoodQuestion3.SetActive(false);
            AIAroundYouQuestion1.SetActive(false);
            AIAroundYouQuestion2.SetActive(false);
            AIForYou.SetActive(false);
            AIForYourBusiness.SetActive(false);
            AIForGood.SetActive(false);
            AIAroundYou.SetActive(false);

            Gesture.GestureForMainMenuAIForYou.SetActive(false);
            Gesture.GestureForMainMenuAIForYourBusiness.SetActive(false);
            Gesture.GestureForMainMenuAIForGood.SetActive(false);
            Gesture.GestureForMainMenuAIAroundYou.SetActive(false);
        }
    }
}
