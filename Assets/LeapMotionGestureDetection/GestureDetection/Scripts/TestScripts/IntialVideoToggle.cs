﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntialVideoToggle : MonoBehaviour
{
    public GameObject FirstVideo;
    public GameObject LoopVideo;
    public GameObject MainMenu;
    public GameObject IdleVideo;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(DeactivateIdleVideo());
        StartCoroutine(ActivateLoopVideo());
        StartCoroutine(ChangeToLoopVideo());
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    IEnumerator ChangeToLoopVideo()
    {
        yield return new WaitForSeconds(47.0f);
        //LoopVideo.SetActive(true);
        
        MainMenu.SetActive(true);
        FirstVideo.SetActive(false);

    }
    IEnumerator ActivateLoopVideo()
    {
        yield return new WaitForSeconds(46.0f);
        LoopVideo.SetActive(true);
    }
    IEnumerator DeactivateIdleVideo()
    {
        yield return new WaitForSeconds(38.0f);
        IdleVideo.SetActive(false);
        FirstVideo.SetActive(true);
    }
}
