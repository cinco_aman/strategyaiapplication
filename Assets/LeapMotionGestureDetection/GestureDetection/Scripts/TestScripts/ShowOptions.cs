﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowOptions : MonoBehaviour
{
    public GameObject Question1Option1;
    public GameObject Question1Option2;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
   
    public void ActivateOptionsQuestion1()
    {
        Question1Option1.SetActive(true);
        Question1Option2.SetActive(true);
    }
}
