﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetTrigger : MonoBehaviour
{
    public GameObject AIToday;
    public GameObject AIBusiness;
    public GameObject AIGood;
    public GameObject AIAroundYou;

    Vector3 originalPos1;
    Vector3 originalPos2;
    Vector3 originalPos3;
    Vector3 originalPos4;
    // Start is called before the first frame update
    void Start()
    {
        originalPos1 = new Vector3(AIToday.transform.position.x, AIToday.transform.position.y, AIToday.transform.position.z);
        originalPos2 = new Vector3(AIBusiness.transform.position.x, AIBusiness.transform.position.y, AIBusiness.transform.position.z);
        originalPos3 = new Vector3(AIGood.transform.position.x, AIGood.transform.position.y, AIGood.transform.position.z);
        originalPos4 = new Vector3(AIAroundYou.transform.position.x, AIAroundYou.transform.position.y, AIAroundYou.transform.position.z);
    }

    private void OnTriggerEnter(Collider other)
    {
        AIToday.transform.position = originalPos1;
        AIBusiness.transform.position = originalPos2;
        AIGood.transform.position = originalPos3;
        AIAroundYou.transform.position = originalPos4;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
