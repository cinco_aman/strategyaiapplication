﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TogglePanel : MonoBehaviour
{

    public GameObject ToggleMainMenu;
    public GameObject ToggleSubMenu;
    public GameObject AITODAY;
    public GameObject AITomorrow;
    public GameObject AIGood;
    public GameObject AIAroundYou;
    public GameObject AIForToday;
    public GameObject AIForTomorrow;
    public GameObject AIForGood;
    public GameObject AIForYourAround;
    //public GameObject GetBothHandGesture;
    //public GameObject GetBothHandGestureForSubmenu;
    public TwoHandGestureController Gesture;
   

    Vector3 originalPos1;
    Vector3 originalPos2;
    Vector3 originalPos3;
    Vector3 originalPos4;

    // Start is called before the first frame update
    void Start()
    {
        originalPos1 = new Vector3(AITODAY.transform.position.x, AITODAY.transform.position.y, AITODAY.transform.position.z);
        originalPos2 = new Vector3(AITomorrow.transform.position.x, AITomorrow.transform.position.y, AITomorrow.transform.position.z);
        originalPos3 = new Vector3(AIGood.transform.position.x, AIGood.transform.position.y, AIGood.transform.position.z);
        originalPos4 = new Vector3(AIAroundYou.transform.position.x, AIAroundYou.transform.position.y, AIAroundYou.transform.position.z);
    }

    // Update is called once per frame
    void Update()
    { 
        
    }

    public void DeactivatingMainMenu()
    {
        ToggleMainMenu.SetActive(false);

    }


    public void ActivatingMainMenu()
    {
        ToggleMainMenu.SetActive(true);
        if (ToggleMainMenu.activeSelf)
        {
            AIForGood.SetActive(false);
            AIForToday.SetActive(false);
            AIForTomorrow.SetActive(false);
            AIForYourAround.SetActive(false);
            //GetBothHandGesture.SetActive(false);

        }
       
    }

    public void ThingsHappenWhenSelectAIGood()
    {
        AIForGood.SetActive(true);
        if (AIForGood.activeSelf)
        {
            ToggleMainMenu.SetActive(false);
            AIForToday.SetActive(false);
            AIForTomorrow.SetActive(false);
            AIForYourAround.SetActive(false);
            Gesture.GestureForMainMenuAIForGood.SetActive(true);
            AITODAY.transform.position = originalPos1;
            AITomorrow.transform.position = originalPos2;
            AIGood.transform.position = originalPos3;
            AIAroundYou.transform.position = originalPos4;
        }
        
       
        
    }

    public void ThingsHappenWhenSelectAIToday()
    {
        AIForToday.SetActive(true);

        if (AIForToday.activeSelf)
        {
            ToggleMainMenu.SetActive(false);
            AIForTomorrow.SetActive(false);
            AIForGood.SetActive(false);
            AIForYourAround.SetActive(false);
            Gesture.GestureForMainMenuAIForYou.SetActive(true);
            AITODAY.transform.position = originalPos1;
            AITomorrow.transform.position = originalPos2;
            AIGood.transform.position = originalPos3;
            AIAroundYou.transform.position = originalPos4;
        }
        
        
        
    }

    public void ThingsHappenWhenSelectAITomorrow()
    {
        AIForTomorrow.SetActive(true);
        if (AIForTomorrow.activeSelf)
        {
            ToggleMainMenu.SetActive(false);
            AIForToday.SetActive(false);
            AIForGood.SetActive(false);
            AIForYourAround.SetActive(false);
            Gesture.GestureForMainMenuAIForYourBusiness.SetActive(true);
            AITODAY.transform.position = originalPos1;
            AITomorrow.transform.position = originalPos2;
            AIGood.transform.position = originalPos3;
            AIAroundYou.transform.position = originalPos4;

        }
    }

    public void ThingsHappenWhenSelectAIAroundYou()
    {
        AIForYourAround.SetActive(true);
        if (AIForYourAround.activeSelf)
        {
            ToggleMainMenu.SetActive(false);
            AIForToday.SetActive(false);
            AIForGood.SetActive(false);
            AIForTomorrow.SetActive(false);
            Gesture.GestureForMainMenuAIAroundYou.SetActive(true);
            AITODAY.transform.position = originalPos1;
            AITomorrow.transform.position = originalPos2;
            AIGood.transform.position = originalPos3;
            AIAroundYou.transform.position = originalPos4;
        }
    }

    public void ThingsHappenWhenTwoHandGestureisActivated()
    {
        ToggleMainMenu.SetActive(true);
        
         AIForGood.SetActive(false);
         AIForToday.SetActive(false);
         AIForTomorrow.SetActive(false);
        AIForYourAround.SetActive(false);
        //GetBothHandGesture.SetActive(false);
        AITODAY.transform.position = originalPos1;
        AITomorrow.transform.position = originalPos2;
        AIGood.transform.position = originalPos3;
        AIAroundYou.transform.position = originalPos4;

    }

    public void ThingsHappenWhenTwoHandGestureForSubMenuIsActivated()
    {
        ToggleSubMenu.SetActive(true);
        if (ToggleSubMenu.activeSelf)
        {
           
            StartCoroutine(ToggleMainTwoHandGesture());
            //GetBothHandGestureForSubmenu.SetActive(false);
            AITODAY.transform.position = originalPos1;
            AITomorrow.transform.position = originalPos2;
            AIGood.transform.position = originalPos3;
            AIAroundYou.transform.position = originalPos4;
        }
    }

    public void PlayVideo1()
    {
        
            ToggleSubMenu.SetActive(false);
            //GetBothHandGesture.SetActive(false);
            //GetBothHandGestureForSubmenu.SetActive(true);

        
       
    }

    public void PlayVideo2()
    {
       
       
            ToggleSubMenu.SetActive(false);
            //GetBothHandGesture.SetActive(false);
            //GetBothHandGestureForSubmenu.SetActive(true);
        
        
    }

    public void PlayVideo3()
    {
        
       
            ToggleSubMenu.SetActive(false);
            //GetBothHandGesture.SetActive(false);
            //GetBothHandGestureForSubmenu.SetActive(true);
        
       
    }

    public void PlayVideo4()
    {
        
        
            ToggleSubMenu.SetActive(false);
            //GetBothHandGesture.SetActive(false);
            //GetBothHandGestureForSubmenu.SetActive(true);
        
    }


    IEnumerator ToggleMainTwoHandGesture()
    {
        
        yield return new WaitForSeconds(3.0f);
        //GetBothHandGesture.SetActive(true);

    }


    public void ActivateAIForTodaySubMenu()
    {
        
    }

    public void ActivateAIForTomorrowSubmenu()
    {
        
    }

    public void ActivateAIForGoodSubmenu()
    {
        
    }
    


}
