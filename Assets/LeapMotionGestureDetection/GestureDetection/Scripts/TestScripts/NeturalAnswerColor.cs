﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeturalAnswerColor : MonoBehaviour
{

    public Material[] materials;
    public Renderer rend;

    private int index = 1;
    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponent<Renderer>();
        rend.enabled = true;
    }


    public void ChangeBackToOriginalColor()
    {
        if (materials.Length == 0)
        {
            return;
        }

        else
        {
            index += 1;

            if (index == materials.Length + 1)
            {
                index = 1;
            }

            print(index);

            rend.sharedMaterial = materials[index - 1];
        }
    }
}
