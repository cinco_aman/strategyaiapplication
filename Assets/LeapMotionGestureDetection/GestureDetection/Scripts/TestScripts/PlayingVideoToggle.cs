﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayingVideoToggle : MonoBehaviour
{

    public GameObject Image1;
    public GameObject Image2;
    public GameObject Image3;
    public GameObject Image4;
    public GameObject Image5;
    public GameObject Image6;
    public GameObject Image7;
    public GameObject Image8;
    public GameObject Image9;
    public GameObject Image10;
    public GameObject Image11;
    public GameObject Image12;
    public GameObject Image13;
    public GameObject Image14;
    public GameObject Image15;
    public GameObject Image16;
    public GameObject Image17;
    public GameObject Image18;
    public GameObject Image19;
    public GameObject Image20;
    public GameObject Image21;
    public GameObject Image22;
    public GameObject Image23;
    public GameObject Image24;
    public GameObject Video1;
    public GameObject Video2;
    public GameObject Video3;
    public GameObject Video4;
    public GameObject Video5;
    public GameObject Video6;
    public GameObject Video7;
    public GameObject Video8;
    public GameObject Video9;
    public GameObject Video10;
    public GameObject Video11;
    public GameObject Video12;
    public GameObject Video13;
    public GameObject Video14;
    public GameObject Video15;
    public GameObject Video16;
    public GameObject Video17;
    public GameObject Video18;
    public GameObject Video19;
    public GameObject Video20;
    public GameObject Video21;
    public GameObject Video22;
    public GameObject Video23;
    public GameObject Video24;
    public GameObject FirstVideo;
    public GameObject IntroVideo;
    public TwoHandGestureController Gesture;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayingVideo1()
    {
        Video1.SetActive(true);
        if (Video1.activeSelf)
        {
            Image1.SetActive(true);

            Image2.SetActive(false);
            Image3.SetActive(false);
            Image4.SetActive(false);
            Image5.SetActive(false);
            Image6.SetActive(false);
            Image7.SetActive(false);
            Image8.SetActive(false);
            Image9.SetActive(false);
            Image10.SetActive(false);
            Image11.SetActive(false);
            Image12.SetActive(false);
            Image13.SetActive(false);
            Image14.SetActive(false);
            Image15.SetActive(false);
            Image16.SetActive(false);
            Image17.SetActive(false);
            Image18.SetActive(false);
            Image19.SetActive(false);
            Image20.SetActive(false);
            Image21.SetActive(false);
            Image22.SetActive(false);
            Image23.SetActive(false);
            Image24.SetActive(false);

            Video2.SetActive(false);
            Video3.SetActive(false);
            Video4.SetActive(false);
            Video5.SetActive(false);
            Video6.SetActive(false);
            Video7.SetActive(false);
            Video8.SetActive(false);
            Video9.SetActive(false);
            Video10.SetActive(false);
            Video11.SetActive(false);
            Video12.SetActive(false);
            Video13.SetActive(false);
            Video14.SetActive(false);
            Video15.SetActive(false);
            Video16.SetActive(false);
            Video17.SetActive(false);
            Video18.SetActive(false);
            Video19.SetActive(false);
            Video20.SetActive(false);
            Video21.SetActive(false);
            Video22.SetActive(false);
            Video23.SetActive(false);
            Video24.SetActive(false);

            IntroVideo.SetActive(false);
            FirstVideo.SetActive(false);
        }
        Gesture.GestureForAIForYouQuestion1.SetActive(true);
        Gesture.SwipeForQuestion1Video1.SetActive(false);
    }
    public void PlayingVideo2()
    {
        Video2.SetActive(true);
        if (Video2.activeSelf)
        {
            Image2.SetActive(true);

            Image1.SetActive(false);
            Image3.SetActive(false);
            Image4.SetActive(false);
            Image5.SetActive(false);
            Image6.SetActive(false);
            Image7.SetActive(false);
            Image8.SetActive(false);
            Image9.SetActive(false);
            Image10.SetActive(false);
            Image11.SetActive(false);
            Image12.SetActive(false);
            Image13.SetActive(false);
            Image14.SetActive(false);
            Image15.SetActive(false);
            Image16.SetActive(false);
            Image17.SetActive(false);
            Image18.SetActive(false);
            Image19.SetActive(false);
            Image20.SetActive(false);
            Image21.SetActive(false);
            Image22.SetActive(false);
            Image23.SetActive(false);
            Image24.SetActive(false);

            Video1.SetActive(false);
            Video3.SetActive(false);
            Video4.SetActive(false);
            Video5.SetActive(false);
            Video6.SetActive(false);
            Video7.SetActive(false);
            Video8.SetActive(false);
            Video9.SetActive(false);
            Video10.SetActive(false);
            Video11.SetActive(false);
            Video12.SetActive(false);
            Video13.SetActive(false);
            Video14.SetActive(false);
            Video15.SetActive(false);
            Video16.SetActive(false);
            Video17.SetActive(false);
            Video18.SetActive(false);
            Video19.SetActive(false);
            Video20.SetActive(false);
            Video21.SetActive(false);
            Video22.SetActive(false);
            Video23.SetActive(false);
            Video24.SetActive(false);

            IntroVideo.SetActive(false);
            FirstVideo.SetActive(false);

        }
        Gesture.GestureForAIForYouQuestion1.SetActive(true);
    }
    public void PlayingVideo3()
    {
        Video3.SetActive(true);
        if (Video3.activeSelf)
        {
            Image1.SetActive(false);
            Image2.SetActive(false);
            Image3.SetActive(false);
            Image4.SetActive(false);
            Image5.SetActive(false);
            Image6.SetActive(false);
            Image7.SetActive(false);
            Image8.SetActive(false);
            Image9.SetActive(false);
            Image10.SetActive(false);
            Image11.SetActive(false);
            Image12.SetActive(false);
            Image13.SetActive(false);
            Image14.SetActive(false);
            Image15.SetActive(false);
            Image16.SetActive(false);
            Image17.SetActive(false);
            Image18.SetActive(false);
            Image19.SetActive(false);
            Image20.SetActive(false);
            Image21.SetActive(false);
            Image22.SetActive(false);
            Image23.SetActive(false);
            Image24.SetActive(false);

            Video2.SetActive(false);
            Video1.SetActive(false);
            Video4.SetActive(false);
            Video5.SetActive(false);
            Video6.SetActive(false);
            Video7.SetActive(false);
            Video8.SetActive(false);
            Video9.SetActive(false);
            Video10.SetActive(false);
            Video11.SetActive(false);
            Video12.SetActive(false);
            Video13.SetActive(false);
            Video14.SetActive(false);
            Video15.SetActive(false);
            Video16.SetActive(false);
            Video17.SetActive(false);
            Video18.SetActive(false);
            Video19.SetActive(false);
            Video20.SetActive(false);
            Video21.SetActive(false);
            Video22.SetActive(false);
            Video23.SetActive(false);
            Video24.SetActive(false);

            IntroVideo.SetActive(false);
            FirstVideo.SetActive(false);

        }

        Gesture.GestureForAIForYouQuestion2.SetActive(true);
        Gesture.SwipeForQuestion2Video1.SetActive(false);
    }
    public void PlayingVideo4()
    {
        Video4.SetActive(true);
        if (Video4.activeSelf)
        {

            Image4.SetActive(true);

            Image1.SetActive(false);
            Image2.SetActive(false);
            Image3.SetActive(false);
            Image5.SetActive(false);
            Image6.SetActive(false);
            Image7.SetActive(false);
            Image8.SetActive(false);
            Image9.SetActive(false);
            Image10.SetActive(false);
            Image11.SetActive(false);
            Image12.SetActive(false);
            Image13.SetActive(false);
            Image14.SetActive(false);
            Image15.SetActive(false);
            Image16.SetActive(false);
            Image17.SetActive(false);
            Image18.SetActive(false);
            Image19.SetActive(false);
            Image20.SetActive(false);
            Image21.SetActive(false);
            Image22.SetActive(false);
            Image23.SetActive(false);
            Image24.SetActive(false);

            Video2.SetActive(false);
            Video3.SetActive(false);
            Video1.SetActive(false);
            Video5.SetActive(false);
            Video6.SetActive(false);
            Video7.SetActive(false);
            Video8.SetActive(false);
            Video9.SetActive(false);
            Video10.SetActive(false);
            Video11.SetActive(false);
            Video12.SetActive(false);
            Video13.SetActive(false);
            Video14.SetActive(false);
            Video15.SetActive(false);
            Video16.SetActive(false);
            Video17.SetActive(false);
            Video18.SetActive(false);
            Video19.SetActive(false);
            Video20.SetActive(false);
            Video21.SetActive(false);
            Video22.SetActive(false);
            Video23.SetActive(false);
            Video24.SetActive(false);

            IntroVideo.SetActive(false);
            FirstVideo.SetActive(false);
        }
        Gesture.GestureForAIForYouQuestion2.SetActive(true);
    }
    public void PlayingVideo5()
    {
        Video5.SetActive(true);
        if (Video5.activeSelf)
        {
            Image1.SetActive(false);
            Image2.SetActive(false);
            Image3.SetActive(false);
            Image4.SetActive(false);    
            Image6.SetActive(false);
            Image7.SetActive(false);
            Image8.SetActive(false);
            Image9.SetActive(false);
            Image10.SetActive(false);
            Image11.SetActive(false);
            Image12.SetActive(false);
            Image13.SetActive(false);
            Image14.SetActive(false);
            Image15.SetActive(false);
            Image16.SetActive(false);
            Image17.SetActive(false);
            Image18.SetActive(false);
            Image19.SetActive(false);
            Image20.SetActive(false);
            Image21.SetActive(false);
            Image22.SetActive(false);
            Image23.SetActive(false);
            Image24.SetActive(false);

            Video2.SetActive(false);
            Video3.SetActive(false);
            Video4.SetActive(false);
            Video1.SetActive(false);
            Video6.SetActive(false);
            Video7.SetActive(false);
            Video8.SetActive(false);
            Video9.SetActive(false);
            Video10.SetActive(false);
            Video11.SetActive(false);
            Video12.SetActive(false);
            Video13.SetActive(false);
            Video14.SetActive(false);
            Video15.SetActive(false);
            Video16.SetActive(false);
            Video17.SetActive(false);
            Video18.SetActive(false);
            Video19.SetActive(false);
            Video20.SetActive(false);
            Video21.SetActive(false);
            Video22.SetActive(false);
            Video23.SetActive(false);
            Video24.SetActive(false);

            IntroVideo.SetActive(false);
            FirstVideo.SetActive(false);
        }

        Gesture.SwipeForQuestion3Video1.SetActive(false);
        Gesture.GestureForAIForYouQuestion3.SetActive(true);
    }
    public void PlayingVideo6()
    {
        Video6.SetActive(true);
        if (Video6.activeSelf)
        {
            Image6.SetActive(true);

            Image1.SetActive(false);
            Image2.SetActive(false);
            Image3.SetActive(false);
            Image4.SetActive(false);
            Image5.SetActive(false);
            Image7.SetActive(false);
            Image8.SetActive(false);
            Image9.SetActive(false);
            Image10.SetActive(false);
            Image11.SetActive(false);
            Image12.SetActive(false);
            Image13.SetActive(false);
            Image14.SetActive(false);
            Image15.SetActive(false);
            Image16.SetActive(false);
            Image17.SetActive(false);
            Image18.SetActive(false);
            Image19.SetActive(false);
            Image20.SetActive(false);
            Image21.SetActive(false);
            Image22.SetActive(false);
            Image23.SetActive(false);
            Image24.SetActive(false);


            Video2.SetActive(false);
            Video3.SetActive(false);
            Video4.SetActive(false);
            Video5.SetActive(false);
            Video1.SetActive(false);
            Video7.SetActive(false);
            Video8.SetActive(false);
            Video9.SetActive(false);
            Video10.SetActive(false);
            Video11.SetActive(false);
            Video12.SetActive(false);
            Video13.SetActive(false);
            Video14.SetActive(false);
            Video15.SetActive(false);
            Video16.SetActive(false);
            Video17.SetActive(false);
            Video18.SetActive(false);
            Video19.SetActive(false);
            Video20.SetActive(false);
            Video21.SetActive(false);
            Video22.SetActive(false);
            Video23.SetActive(false);
            Video24.SetActive(false);

            IntroVideo.SetActive(false);
            FirstVideo.SetActive(false);
        }
        Gesture.GestureForAIForYouQuestion3.SetActive(true);
    }
    public void PlayingVideo7()
    {
        Video7.SetActive(true);
        if (Video7.activeSelf)
        {
            Image1.SetActive(false);
            Image2.SetActive(false);
            Image3.SetActive(false);
            Image4.SetActive(false);
            Image5.SetActive(false);
            Image6.SetActive(false);
            Image8.SetActive(false);
            Image9.SetActive(false);
            Image10.SetActive(false);
            Image11.SetActive(false);
            Image12.SetActive(false);
            Image13.SetActive(false);
            Image14.SetActive(false);
            Image15.SetActive(false);
            Image16.SetActive(false);
            Image17.SetActive(false);
            Image18.SetActive(false);
            Image19.SetActive(false);
            Image20.SetActive(false);
            Image21.SetActive(false);
            Image22.SetActive(false);
            Image23.SetActive(false);
            Image24.SetActive(false);

            Video2.SetActive(false);
            Video3.SetActive(false);
            Video4.SetActive(false);
            Video5.SetActive(false);
            Video6.SetActive(false);
            Video1.SetActive(false);
            Video8.SetActive(false);
            Video9.SetActive(false);
            Video10.SetActive(false);
            Video11.SetActive(false);
            Video12.SetActive(false);
            Video13.SetActive(false);
            Video14.SetActive(false);
            Video15.SetActive(false);
            Video16.SetActive(false);
            Video17.SetActive(false);
            Video18.SetActive(false);
            Video19.SetActive(false);
            Video20.SetActive(false);
            Video21.SetActive(false);
            Video22.SetActive(false);
            Video23.SetActive(false);
            Video24.SetActive(false);

            IntroVideo.SetActive(false);
            FirstVideo.SetActive(false);

            Gesture.SwipeForQuestion4Video1.SetActive(false);
        }

        //Gesture.SwipeForQuestion4Video1.SetActive(false);
    }
    public void PlayingVideo8()
    {
        Video8.SetActive(true);
        if (Video8.activeSelf)
        {
            Image8.SetActive(true);

            Image1.SetActive(false);
            Image2.SetActive(false);
            Image3.SetActive(false);
            Image4.SetActive(false);
            Image5.SetActive(false);
            Image6.SetActive(false);
            Image7.SetActive(false);
           
            Image9.SetActive(false);
            Image10.SetActive(false);
            Image11.SetActive(false);
            Image12.SetActive(false);
            Image13.SetActive(false);
            Image14.SetActive(false);
            Image15.SetActive(false);
            Image16.SetActive(false);
            Image17.SetActive(false);
            Image18.SetActive(false);
            Image19.SetActive(false);
            Image20.SetActive(false);
            Image21.SetActive(false);
            Image22.SetActive(false);
            Image23.SetActive(false);
            Image24.SetActive(false);

            Video2.SetActive(false);
            Video3.SetActive(false);
            Video4.SetActive(false);
            Video5.SetActive(false);
            Video6.SetActive(false);
            Video7.SetActive(false);
            Video1.SetActive(false);
            Video9.SetActive(false);
            Video10.SetActive(false);
            Video11.SetActive(false);
            Video12.SetActive(false);
            Video13.SetActive(false);
            Video14.SetActive(false);
            Video15.SetActive(false);
            Video16.SetActive(false);
            Video17.SetActive(false);
            Video18.SetActive(false);
            Video19.SetActive(false);
            Video20.SetActive(false);
            Video21.SetActive(false);
            Video22.SetActive(false);
            Video23.SetActive(false);
            Video24.SetActive(false);

            IntroVideo.SetActive(false);
            FirstVideo.SetActive(false);
        }

        
    }
    public void PlayingVideo9()
    {
        Video9.SetActive(true);
        if (Video9.activeSelf)
        {
            Image1.SetActive(false);
            Image2.SetActive(false);
            Image3.SetActive(false);
            Image4.SetActive(false);
            Image5.SetActive(false);
            Image6.SetActive(false);
            Image7.SetActive(false);
            Image8.SetActive(false);
            Image10.SetActive(false);
            Image11.SetActive(false);
            Image12.SetActive(false);
            Image13.SetActive(false);
            Image14.SetActive(false);
            Image15.SetActive(false);
            Image16.SetActive(false);
            Image17.SetActive(false);
            Image18.SetActive(false);
            Image19.SetActive(false);
            Image20.SetActive(false);
            Image21.SetActive(false);
            Image22.SetActive(false);
            Image23.SetActive(false);
            Image24.SetActive(false);

            Video2.SetActive(false);
            Video3.SetActive(false);
            Video4.SetActive(false);
            Video5.SetActive(false);
            Video6.SetActive(false);
            Video7.SetActive(false);
            Video8.SetActive(false);
            Video1.SetActive(false);
            Video10.SetActive(false);
            Video11.SetActive(false);
            Video12.SetActive(false);
            Video13.SetActive(false);
            Video14.SetActive(false);
            Video15.SetActive(false);
            Video16.SetActive(false);
            Video17.SetActive(false);
            Video18.SetActive(false);
            Video19.SetActive(false);
            Video20.SetActive(false);
            Video21.SetActive(false);
            Video22.SetActive(false);
            Video23.SetActive(false);
            Video24.SetActive(false);

            IntroVideo.SetActive(false);
            FirstVideo.SetActive(false);
        }

        Gesture.SwipeForQuestion5Video1.SetActive(false);
    }
    public void PlayingVideo10()
    {
        Video10.SetActive(true);
        if (Video10.activeSelf)
        {
            Image10.SetActive(true);

            Image1.SetActive(false);
            Image2.SetActive(false);
            Image3.SetActive(false);
            Image4.SetActive(false);
            Image5.SetActive(false);
            Image6.SetActive(false);
            Image7.SetActive(false);
            Image8.SetActive(false);
            Image9.SetActive(false);
            Image11.SetActive(false);
            Image12.SetActive(false);
            Image13.SetActive(false);
            Image14.SetActive(false);
            Image15.SetActive(false);
            Image16.SetActive(false);
            Image17.SetActive(false);
            Image18.SetActive(false);
            Image19.SetActive(false);
            Image20.SetActive(false);
            Image21.SetActive(false);
            Image22.SetActive(false);
            Image23.SetActive(false);
            Image24.SetActive(false);

            Video2.SetActive(false);
            Video3.SetActive(false);
            Video4.SetActive(false);
            Video5.SetActive(false);
            Video6.SetActive(false);
            Video7.SetActive(false);
            Video8.SetActive(false);
            Video9.SetActive(false);
            Video1.SetActive(false);
            Video11.SetActive(false);
            Video12.SetActive(false);
            Video13.SetActive(false);
            Video14.SetActive(false);
            Video15.SetActive(false);
            Video16.SetActive(false);
            Video17.SetActive(false);
            Video18.SetActive(false);
            Video19.SetActive(false);
            Video20.SetActive(false);
            Video21.SetActive(false);
            Video22.SetActive(false);
            Video23.SetActive(false);
            Video24.SetActive(false);


            IntroVideo.SetActive(false);
            FirstVideo.SetActive(false);

        }
    }
    public void PlayingVideo11()
    {
        Video11.SetActive(true);
        if (Video11.activeSelf)
        {
            Image1.SetActive(false);
            Image2.SetActive(false);
            Image3.SetActive(false);
            Image4.SetActive(false);
            Image5.SetActive(false);
            Image6.SetActive(false);
            Image7.SetActive(false);
            Image8.SetActive(false);
            Image9.SetActive(false);
            Image10.SetActive(false);

            Image12.SetActive(false);
            Image13.SetActive(false);
            Image14.SetActive(false);
            Image15.SetActive(false);
            Image16.SetActive(false);
            Image17.SetActive(false);
            Image18.SetActive(false);
            Image19.SetActive(false);
            Image20.SetActive(false);
            Image21.SetActive(false);
            Image22.SetActive(false);
            Image23.SetActive(false);
            Image24.SetActive(false);


            Video2.SetActive(false);
            Video3.SetActive(false);
            Video4.SetActive(false);
            Video5.SetActive(false);
            Video6.SetActive(false);
            Video7.SetActive(false);
            Video8.SetActive(false);
            Video9.SetActive(false);
            Video10.SetActive(false);
            Video1.SetActive(false);
            Video12.SetActive(false);
            Video13.SetActive(false);
            Video14.SetActive(false);
            Video15.SetActive(false);
            Video16.SetActive(false);
            Video17.SetActive(false);
            Video18.SetActive(false);
            Video19.SetActive(false);
            Video20.SetActive(false);
            Video21.SetActive(false);
            Video22.SetActive(false);
            Video23.SetActive(false);
            Video24.SetActive(false);

            IntroVideo.SetActive(false);
            FirstVideo.SetActive(false);

        }

        Gesture.SwipeForQuestion6Video1.SetActive(false);
    }
    public void PlayingVideo12()
    {
        Video12.SetActive(true);
        if (Video12.activeSelf)
        {
            Image12.SetActive(true);

            Image1.SetActive(false);
            Image2.SetActive(false);
            Image3.SetActive(false);
            Image4.SetActive(false);
            Image5.SetActive(false);
            Image6.SetActive(false);
            Image7.SetActive(false);
            Image8.SetActive(false);
            Image9.SetActive(false);
            Image10.SetActive(false);
            Image11.SetActive(false);
            Image13.SetActive(false);
            Image14.SetActive(false);
            Image15.SetActive(false);
            Image16.SetActive(false);
            Image17.SetActive(false);
            Image18.SetActive(false);
            Image19.SetActive(false);
            Image20.SetActive(false);
            Image21.SetActive(false);
            Image22.SetActive(false);
            Image23.SetActive(false);
            Image24.SetActive(false);

            Video2.SetActive(false);
            Video3.SetActive(false);
            Video4.SetActive(false);
            Video5.SetActive(false);
            Video6.SetActive(false);
            Video7.SetActive(false);
            Video8.SetActive(false);
            Video9.SetActive(false);
            Video10.SetActive(false);
            Video11.SetActive(false);
            Video1.SetActive(false);
            Video13.SetActive(false);
            Video14.SetActive(false);
            Video15.SetActive(false);
            Video16.SetActive(false);
            Video17.SetActive(false);
            Video18.SetActive(false);
            Video19.SetActive(false);
            Video20.SetActive(false);
            Video21.SetActive(false);
            Video22.SetActive(false);
            Video23.SetActive(false);
            Video24.SetActive(false);

            IntroVideo.SetActive(false);
            FirstVideo.SetActive(false);

        }
    }
    public void PlayingVideo13()
    {
        Video13.SetActive(true);
        if (Video13.activeSelf)
        {
            Image1.SetActive(false);
            Image2.SetActive(false);
            Image3.SetActive(false);
            Image4.SetActive(false);
            Image5.SetActive(false);
            Image6.SetActive(false);
            Image7.SetActive(false);
            Image8.SetActive(false);
            Image9.SetActive(false);
            Image10.SetActive(false);
            Image11.SetActive(false);
            Image12.SetActive(false);

            Image14.SetActive(false);
            Image15.SetActive(false);
            Image16.SetActive(false);
            Image17.SetActive(false);
            Image18.SetActive(false);
            Image19.SetActive(false);
            Image20.SetActive(false);
            Image21.SetActive(false);
            Image22.SetActive(false);
            Image23.SetActive(false);
            Image24.SetActive(false);


            Video2.SetActive(false);
            Video3.SetActive(false);
            Video4.SetActive(false);
            Video5.SetActive(false);
            Video6.SetActive(false);
            Video7.SetActive(false);
            Video8.SetActive(false);
            Video9.SetActive(false);
            Video10.SetActive(false);
            Video11.SetActive(false);
            Video12.SetActive(false);
            Video1.SetActive(false);
            Video14.SetActive(false);
            Video15.SetActive(false);
            Video16.SetActive(false);
            Video17.SetActive(false);
            Video18.SetActive(false);
            Video19.SetActive(false);
            Video20.SetActive(false);
            Video21.SetActive(false);
            Video22.SetActive(false);
            Video23.SetActive(false);
            Video24.SetActive(false);

            IntroVideo.SetActive(false);
            FirstVideo.SetActive(false);

        }

        Gesture.SwipeForQuestion7Video1.SetActive(false);
    }
    public void PlayingVideo14()
    {
        Video14.SetActive(true);
        if (Video14.activeSelf)
        {
            Image14.SetActive(true);

            Image1.SetActive(false);
            Image2.SetActive(false);
            Image3.SetActive(false);
            Image4.SetActive(false);
            Image5.SetActive(false);
            Image6.SetActive(false);
            Image7.SetActive(false);
            Image8.SetActive(false);
            Image9.SetActive(false);
            Image10.SetActive(false);
            Image11.SetActive(false);
            Image12.SetActive(false);
            Image13.SetActive(false);
            Image15.SetActive(false);
            Image16.SetActive(false);
            Image17.SetActive(false);
            Image18.SetActive(false);
            Image19.SetActive(false);
            Image20.SetActive(false);
            Image21.SetActive(false);
            Image22.SetActive(false);
            Image23.SetActive(false);
            Image24.SetActive(false);

            Video2.SetActive(false);
            Video3.SetActive(false);
            Video4.SetActive(false);
            Video5.SetActive(false);
            Video6.SetActive(false);
            Video7.SetActive(false);
            Video8.SetActive(false);
            Video9.SetActive(false);
            Video10.SetActive(false);
            Video11.SetActive(false);
            Video12.SetActive(false);
            Video13.SetActive(false);
            Video1.SetActive(false);
            Video15.SetActive(false);
            Video16.SetActive(false);
            Video17.SetActive(false);
            Video18.SetActive(false);
            Video19.SetActive(false);
            Video20.SetActive(false);
            Video21.SetActive(false);
            Video22.SetActive(false);
            Video23.SetActive(false);
            Video24.SetActive(false);

            IntroVideo.SetActive(false);
            FirstVideo.SetActive(false);

        }
    }
    public void PlayingVideo15()
    {
        Video15.SetActive(true);
        if (Video15.activeSelf)
        {
            Image1.SetActive(false);
            Image2.SetActive(false);
            Image3.SetActive(false);
            Image4.SetActive(false);
            Image5.SetActive(false);
            Image6.SetActive(false);
            Image7.SetActive(false);
            Image8.SetActive(false);
            Image9.SetActive(false);
            Image10.SetActive(false);
            Image11.SetActive(false);
            Image12.SetActive(false);
            Image13.SetActive(false);
            Image14.SetActive(false);
            Image16.SetActive(false);
            Image17.SetActive(false);
            Image18.SetActive(false);
            Image19.SetActive(false);
            Image20.SetActive(false);
            Image21.SetActive(false);
            Image22.SetActive(false);
            Image23.SetActive(false);
            Image24.SetActive(false);

            Video2.SetActive(false);
            Video3.SetActive(false);
            Video4.SetActive(false);
            Video5.SetActive(false);
            Video6.SetActive(false);
            Video7.SetActive(false);
            Video8.SetActive(false);
            Video9.SetActive(false);
            Video10.SetActive(false);
            Video11.SetActive(false);
            Video12.SetActive(false);
            Video13.SetActive(false);
            Video14.SetActive(false);
            Video1.SetActive(false);
            Video16.SetActive(false);
            Video17.SetActive(false);
            Video18.SetActive(false);
            Video19.SetActive(false);
            Video20.SetActive(false);
            Video21.SetActive(false);
            Video22.SetActive(false);
            Video23.SetActive(false);
            Video24.SetActive(false);

            IntroVideo.SetActive(false);
            FirstVideo.SetActive(false);

        }

        Gesture.SwipeForQuestion8Video1.SetActive(false);
    }
    public void PlayingVideo16()
    {
        Video16.SetActive(true);
        if (Video16.activeSelf)
        {
            Image16.SetActive(true);

            Image1.SetActive(false);
            Image2.SetActive(false);
            Image3.SetActive(false);
            Image4.SetActive(false);
            Image5.SetActive(false);
            Image6.SetActive(false);
            Image7.SetActive(false);
            Image8.SetActive(false);
            Image9.SetActive(false);
            Image10.SetActive(false);
            Image11.SetActive(false);
            Image12.SetActive(false);
            Image13.SetActive(false);
            Image14.SetActive(false);
            Image15.SetActive(false);
            Image17.SetActive(false);
            Image18.SetActive(false);
            Image19.SetActive(false);
            Image20.SetActive(false);
            Image21.SetActive(false);
            Image22.SetActive(false);
            Image23.SetActive(false);
            Image24.SetActive(false);

            Video2.SetActive(false);
            Video3.SetActive(false);
            Video4.SetActive(false);
            Video5.SetActive(false);
            Video6.SetActive(false);
            Video7.SetActive(false);
            Video8.SetActive(false);
            Video9.SetActive(false);
            Video10.SetActive(false);
            Video11.SetActive(false);
            Video12.SetActive(false);
            Video13.SetActive(false);
            Video14.SetActive(false);
            Video15.SetActive(false);
            Video1.SetActive(false);
            Video17.SetActive(false);
            Video18.SetActive(false);
            Video19.SetActive(false);
            Video20.SetActive(false);
            Video21.SetActive(false);
            Video22.SetActive(false);
            Video23.SetActive(false);
            Video24.SetActive(false);

            IntroVideo.SetActive(false);
            FirstVideo.SetActive(false);
        }
    }
    public void PlayingVideo17()
    {
        Video17.SetActive(true);
        if (Video17.activeSelf)
        {
            Image1.SetActive(false);
            Image2.SetActive(false);
            Image3.SetActive(false);
            Image4.SetActive(false);
            Image5.SetActive(false);
            Image6.SetActive(false);
            Image7.SetActive(false);
            Image8.SetActive(false);
            Image9.SetActive(false);
            Image10.SetActive(false);
            Image11.SetActive(false);
            Image12.SetActive(false);
            Image13.SetActive(false);
            Image14.SetActive(false);
            Image15.SetActive(false);
            Image16.SetActive(false);
            Image18.SetActive(false);
            Image19.SetActive(false);
            Image20.SetActive(false);
            Image21.SetActive(false);
            Image22.SetActive(false);
            Image23.SetActive(false);
            Image24.SetActive(false);

            Video2.SetActive(false);
            Video3.SetActive(false);
            Video4.SetActive(false);
            Video5.SetActive(false);
            Video6.SetActive(false);
            Video7.SetActive(false);
            Video8.SetActive(false);
            Video9.SetActive(false);
            Video10.SetActive(false);
            Video11.SetActive(false);
            Video12.SetActive(false);
            Video13.SetActive(false);
            Video14.SetActive(false);
            Video15.SetActive(false);
            Video16.SetActive(false);
            Video1.SetActive(false);
            Video18.SetActive(false);
            Video19.SetActive(false);
            Video20.SetActive(false);
            Video21.SetActive(false);
            Video22.SetActive(false);
            Video23.SetActive(false);
            Video24.SetActive(false);

            IntroVideo.SetActive(false);
            FirstVideo.SetActive(false);

        }

        Gesture.SwipeForQuestion9Video1.SetActive(false);
    }
    public void PlayingVideo18()
    {
        Video18.SetActive(true);
        if (Video18.activeSelf)
        {
            Image18.SetActive(true);

            Image1.SetActive(false);
            Image2.SetActive(false);
            Image3.SetActive(false);
            Image4.SetActive(false);
            Image5.SetActive(false);
            Image6.SetActive(false);
            Image7.SetActive(false);
            Image8.SetActive(false);
            Image9.SetActive(false);
            Image10.SetActive(false);
            Image11.SetActive(false);
            Image12.SetActive(false);
            Image13.SetActive(false);
            Image14.SetActive(false);
            Image15.SetActive(false);
            Image16.SetActive(false);
            Image17.SetActive(false);
            Image19.SetActive(false);
            Image20.SetActive(false);
            Image21.SetActive(false);
            Image22.SetActive(false);
            Image23.SetActive(false);
            Image24.SetActive(false);

            Video2.SetActive(false);
            Video3.SetActive(false);
            Video4.SetActive(false);
            Video5.SetActive(false);
            Video6.SetActive(false);
            Video7.SetActive(false);
            Video8.SetActive(false);
            Video9.SetActive(false);
            Video10.SetActive(false);
            Video11.SetActive(false);
            Video12.SetActive(false);
            Video13.SetActive(false);
            Video14.SetActive(false);
            Video15.SetActive(false);
            Video16.SetActive(false);
            Video17.SetActive(false);
            Video1.SetActive(false);
            Video19.SetActive(false);
            Video20.SetActive(false);
            Video21.SetActive(false);
            Video22.SetActive(false);
            Video23.SetActive(false);
            Video24.SetActive(false);

            IntroVideo.SetActive(false);
            FirstVideo.SetActive(false);

        }
    }
    public void PlayingVideo19()
    {
        Video19.SetActive(true);
        if (Video19.activeSelf)
        {

            Image1.SetActive(false);
            Image2.SetActive(false);
            Image3.SetActive(false);
            Image4.SetActive(false);
            Image5.SetActive(false);
            Image6.SetActive(false);
            Image7.SetActive(false);
            Image8.SetActive(false);
            Image9.SetActive(false);
            Image10.SetActive(false);
            Image11.SetActive(false);
            Image12.SetActive(false);
            Image13.SetActive(false);
            Image14.SetActive(false);
            Image15.SetActive(false);
            Image16.SetActive(false);
            Image17.SetActive(false);
            Image18.SetActive(false);

            Image20.SetActive(false);
            Image21.SetActive(false);
            Image22.SetActive(false);
            Image23.SetActive(false);
            Image24.SetActive(false);

            Video2.SetActive(false);
            Video3.SetActive(false);
            Video4.SetActive(false);
            Video5.SetActive(false);
            Video6.SetActive(false);
            Video7.SetActive(false);
            Video8.SetActive(false);
            Video9.SetActive(false);
            Video10.SetActive(false);
            Video11.SetActive(false);
            Video12.SetActive(false);
            Video13.SetActive(false);
            Video14.SetActive(false);
            Video15.SetActive(false);
            Video16.SetActive(false);
            Video17.SetActive(false);
            Video18.SetActive(false);
            Video1.SetActive(false);
            Video20.SetActive(false);
            Video21.SetActive(false);
            Video22.SetActive(false);
            Video23.SetActive(false);
            Video24.SetActive(false);

            IntroVideo.SetActive(false);
            FirstVideo.SetActive(false);

        }

        Gesture.SwipeForQuestion10Video1.SetActive(false);
    }
    public void PlayingVideo20()
    {
        Video20.SetActive(true);
        if (Video1.activeSelf)
        {
            Image20.SetActive(true);

            Image1.SetActive(false);
            Image2.SetActive(false);
            Image3.SetActive(false);
            Image4.SetActive(false);
            Image5.SetActive(false);
            Image6.SetActive(false);
            Image7.SetActive(false);
            Image8.SetActive(false);
            Image9.SetActive(false);
            Image10.SetActive(false);
            Image11.SetActive(false);
            Image12.SetActive(false);
            Image13.SetActive(false);
            Image14.SetActive(false);
            Image15.SetActive(false);
            Image16.SetActive(false);
            Image17.SetActive(false);
            Image18.SetActive(false);
            Image19.SetActive(false);

            Image21.SetActive(false);
            Image22.SetActive(false);
            Image23.SetActive(false);
            Image24.SetActive(false);

            Video2.SetActive(false);
            Video3.SetActive(false);
            Video4.SetActive(false);
            Video5.SetActive(false);
            Video6.SetActive(false);
            Video7.SetActive(false);
            Video8.SetActive(false);
            Video9.SetActive(false);
            Video10.SetActive(false);
            Video11.SetActive(false);
            Video12.SetActive(false);
            Video13.SetActive(false);
            Video14.SetActive(false);
            Video15.SetActive(false);
            Video16.SetActive(false);
            Video17.SetActive(false);
            Video18.SetActive(false);
            Video19.SetActive(false);
            Video1.SetActive(false);
            Video21.SetActive(false);
            Video22.SetActive(false);
            Video23.SetActive(false);
            Video24.SetActive(false);

            IntroVideo.SetActive(false);
            FirstVideo.SetActive(false);

        }
    }
    public void PlayingVideo21()
    {
        Video21.SetActive(true);
        if (Video21.activeSelf)
        {

            Image1.SetActive(false);
            Image2.SetActive(false);
            Image3.SetActive(false);
            Image4.SetActive(false);
            Image5.SetActive(false);
            Image6.SetActive(false);
            Image7.SetActive(false);
            Image8.SetActive(false);
            Image9.SetActive(false);
            Image10.SetActive(false);
            Image11.SetActive(false);
            Image12.SetActive(false);
            Image13.SetActive(false);
            Image14.SetActive(false);
            Image15.SetActive(false);
            Image16.SetActive(false);
            Image17.SetActive(false);
            Image18.SetActive(false);
            Image19.SetActive(false);
            Image20.SetActive(false);
            Image22.SetActive(false);
            Image23.SetActive(false);
            Image24.SetActive(false);

            Video2.SetActive(false);
            Video3.SetActive(false);
            Video4.SetActive(false);
            Video5.SetActive(false);
            Video6.SetActive(false);
            Video7.SetActive(false);
            Video8.SetActive(false);
            Video9.SetActive(false);
            Video10.SetActive(false);
            Video11.SetActive(false);
            Video12.SetActive(false);
            Video13.SetActive(false);
            Video14.SetActive(false);
            Video15.SetActive(false);
            Video16.SetActive(false);
            Video17.SetActive(false);
            Video18.SetActive(false);
            Video19.SetActive(false);
            Video20.SetActive(false);
            Video1.SetActive(false);
            Video22.SetActive(false);
            Video23.SetActive(false);
            Video24.SetActive(false);

            IntroVideo.SetActive(false);
            FirstVideo.SetActive(false);

        }

        Gesture.SwipeForQuestion11Video1.SetActive(false);
    }
    public void PlayingVideo22()
    {
        Video22.SetActive(true);
        if (Video22.activeSelf)
        {
            Image22.SetActive(true);

            Image1.SetActive(false);
            Image2.SetActive(false);
            Image3.SetActive(false);
            Image4.SetActive(false);
            Image5.SetActive(false);
            Image6.SetActive(false);
            Image7.SetActive(false);
            Image8.SetActive(false);
            Image9.SetActive(false);
            Image10.SetActive(false);
            Image11.SetActive(false);
            Image12.SetActive(false);
            Image13.SetActive(false);
            Image14.SetActive(false);
            Image15.SetActive(false);
            Image16.SetActive(false);
            Image17.SetActive(false);
            Image18.SetActive(false);
            Image19.SetActive(false);
            Image20.SetActive(false);
            Image21.SetActive(false);
            Image23.SetActive(false);
            Image24.SetActive(false);

            Video2.SetActive(false);
            Video3.SetActive(false);
            Video4.SetActive(false);
            Video5.SetActive(false);
            Video6.SetActive(false);
            Video7.SetActive(false);
            Video8.SetActive(false);
            Video9.SetActive(false);
            Video10.SetActive(false);
            Video11.SetActive(false);
            Video12.SetActive(false);
            Video13.SetActive(false);
            Video14.SetActive(false);
            Video15.SetActive(false);
            Video16.SetActive(false);
            Video17.SetActive(false);
            Video18.SetActive(false);
            Video19.SetActive(false);
            Video20.SetActive(false);
            Video21.SetActive(false);
            Video1.SetActive(false);
            Video23.SetActive(false);
            Video24.SetActive(false);

            IntroVideo.SetActive(false);
            FirstVideo.SetActive(false);

        }
    }
    public void PlayingVideo23()
    {
        Video23.SetActive(true);
        if (Video23.activeSelf)
        {
            Image1.SetActive(false);
            Image2.SetActive(false);
            Image3.SetActive(false);
            Image4.SetActive(false);
            Image5.SetActive(false);
            Image6.SetActive(false);
            Image7.SetActive(false);
            Image8.SetActive(false);
            Image9.SetActive(false);
            Image10.SetActive(false);
            Image11.SetActive(false);
            Image12.SetActive(false);
            Image13.SetActive(false);
            Image14.SetActive(false);
            Image15.SetActive(false);
            Image16.SetActive(false);
            Image17.SetActive(false);
            Image18.SetActive(false);
            Image19.SetActive(false);
            Image20.SetActive(false);
            Image21.SetActive(false);
            Image22.SetActive(false);
            Image24.SetActive(false);

            Video2.SetActive(false);
            Video3.SetActive(false);
            Video4.SetActive(false);
            Video5.SetActive(false);
            Video6.SetActive(false);
            Video7.SetActive(false);
            Video8.SetActive(false);
            Video9.SetActive(false);
            Video10.SetActive(false);
            Video11.SetActive(false);
            Video12.SetActive(false);
            Video13.SetActive(false);
            Video14.SetActive(false);
            Video15.SetActive(false);
            Video16.SetActive(false);
            Video17.SetActive(false);
            Video18.SetActive(false);
            Video19.SetActive(false);
            Video20.SetActive(false);
            Video21.SetActive(false);
            Video22.SetActive(false);
            Video1.SetActive(false);
            Video24.SetActive(false);

            IntroVideo.SetActive(false);
            FirstVideo.SetActive(false);

        }

        Gesture.SwipeForQuestion12Video1.SetActive(false);
    }
    public void PlayingVideo24()
    {
        Video24.SetActive(true);
        if (Video24.activeSelf)
        {
            Image24.SetActive(true);

            Image1.SetActive(false);
            Image2.SetActive(false);
            Image3.SetActive(false);
            Image4.SetActive(false);
            Image5.SetActive(false);
            Image6.SetActive(false);
            Image7.SetActive(false);
            Image8.SetActive(false);
            Image9.SetActive(false);
            Image10.SetActive(false);
            Image11.SetActive(false);
            Image12.SetActive(false);
            Image13.SetActive(false);
            Image14.SetActive(false);
            Image15.SetActive(false);
            Image16.SetActive(false);
            Image17.SetActive(false);
            Image18.SetActive(false);
            Image19.SetActive(false);
            Image20.SetActive(false);
            Image21.SetActive(false);
            Image22.SetActive(false);
            Image23.SetActive(false);
            

            Video2.SetActive(false);
            Video3.SetActive(false);
            Video4.SetActive(false);
            Video5.SetActive(false);
            Video6.SetActive(false);
            Video7.SetActive(false);
            Video8.SetActive(false);
            Video9.SetActive(false);
            Video10.SetActive(false);
            Video11.SetActive(false);
            Video12.SetActive(false);
            Video13.SetActive(false);
            Video14.SetActive(false);
            Video15.SetActive(false);
            Video16.SetActive(false);
            Video17.SetActive(false);
            Video18.SetActive(false);
            Video19.SetActive(false);
            Video20.SetActive(false);
            Video21.SetActive(false);
            Video22.SetActive(false);
            Video23.SetActive(false);
            Video1.SetActive(false);

            IntroVideo.SetActive(false);
            FirstVideo.SetActive(false);

        }
    }
}
