﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]

public class HandDetector : MonoBehaviour
{
    private bool onHand;
    private EHand targetHand = EHand.eRightHand;
    private List<GameObject> objectsToIgnore;
    
    void Start()
    {
        onHand = false;
        objectsToIgnore = new List<GameObject>();

        var draggables = GameObject.FindObjectsOfType<Draggable>();

        foreach (var drag in draggables) {
            objectsToIgnore.Add(drag.gameObject);
        }
    }
    
    private void OnTriggerEnter(Collider other)
    {
        foreach (GameObject obj in objectsToIgnore)
        {
            if (obj.Equals(other.gameObject))
            {
                return;
            }              
        }
        
        onHand = true;
    }

    public void SetHand(EHand newHand)
    {
        targetHand = newHand;
    }

    private void OnTriggerExit(Collider other)
    {
        foreach (GameObject obj in objectsToIgnore)
        {
            if (obj.Equals(other.gameObject))
            {
                return;
            }                      
        }
        
        onHand = false;
    }

    public bool IsOnHand() => onHand;
}
