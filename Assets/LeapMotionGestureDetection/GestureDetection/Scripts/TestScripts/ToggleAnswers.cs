﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleAnswers : MonoBehaviour
{
    public GameObject Answer1;
    public GameObject Answer2;
    public GameObject Answer3;
    public GameObject Answer4;
    public GameObject Answer5;
    public GameObject Answer6;
    public GameObject Answer7;
    public GameObject Answer8;
    public GameObject Answer9;
    public GameObject Answer10;
    public GameObject Answer11;
    public GameObject Answer12;
    public GameObject Question1;
    public GameObject Question2;
    public GameObject Question3;
    public GameObject Question4;
    public GameObject Question5;
    public GameObject Question6;
    public GameObject Question7;
    public GameObject Question8;
    public GameObject Question9;
    public GameObject Question10;
    public GameObject Question11;
    public GameObject Question12;
    public GameObject AIForYou;
    public GameObject AIForYourBusiness;
    public GameObject AIForGood;
    public GameObject AIAroundYou;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TogglingAnswer()
    {
        Question1.SetActive(false);
        Question2.SetActive(false);
        Question3.SetActive(false);
        Question4.SetActive(false);
        Question5.SetActive(false);
        Question6.SetActive(false);
        Question7.SetActive(false);
        Question8.SetActive(false);
        Question9.SetActive(false);
        Question10.SetActive(false);
        Question11.SetActive(false);
        Question12.SetActive(false);
        Answer1.SetActive(false);
        Answer2.SetActive(false);
        Answer3.SetActive(false);
        Answer4.SetActive(false);
        Answer5.SetActive(false);
        Answer6.SetActive(false);
        Answer7.SetActive(false);
        Answer8.SetActive(false);
        Answer9.SetActive(false);
        Answer10.SetActive(false);
        Answer11.SetActive(false);
        Answer12.SetActive(false);
    }

    public void TogglingAnswerForAIForYourBusiness()
    {
        AIForYou.SetActive(false);
        AIForYourBusiness.SetActive(true);
        AIForGood.SetActive(false);
        AIAroundYou.SetActive(false);
        Answer1.SetActive(false);
        Answer2.SetActive(false);
        Answer3.SetActive(false);
        Answer4.SetActive(false);
        Answer5.SetActive(false);
        Answer6.SetActive(false);
        Answer7.SetActive(false);
        Answer8.SetActive(false);
        Answer9.SetActive(false);
        Answer10.SetActive(false);
        Answer11.SetActive(false);
        Answer12.SetActive(false);
    }

    public void TogglingAnswerForAIForGood()
    {
        AIForYou.SetActive(false);
        AIForYourBusiness.SetActive(false);
        AIForGood.SetActive(true);
        AIAroundYou.SetActive(false);
        Answer1.SetActive(false);
        Answer2.SetActive(false);
        Answer3.SetActive(false);
        Answer4.SetActive(false);
        Answer5.SetActive(false);
        Answer6.SetActive(false);
        Answer7.SetActive(false);
        Answer8.SetActive(false);
        Answer9.SetActive(false);
        Answer10.SetActive(false);
        Answer11.SetActive(false);
        Answer12.SetActive(false);
    }

    public void TogglingAnswerForAIAroundYou()
    {
        AIForYou.SetActive(false);
        AIForYourBusiness.SetActive(false);
        AIForGood.SetActive(false);
        AIAroundYou.SetActive(true);
        Answer1.SetActive(false);
        Answer2.SetActive(false);
        Answer3.SetActive(false);
        Answer4.SetActive(false);
        Answer5.SetActive(false);
        Answer6.SetActive(false);
        Answer7.SetActive(false);
        Answer8.SetActive(false);
        Answer9.SetActive(false);
        Answer10.SetActive(false);
        Answer11.SetActive(false);
        Answer12.SetActive(false);
    }
}
