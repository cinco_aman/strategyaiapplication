﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.SceneManagement;

public class Trigger : MonoBehaviour
{

    public TogglePanel toggle1;
    public GameObject AIToday;
    public GameObject AITomorrow;
    public GameObject AIGood;
    public GameObject AIAroundYou;
    public GameObject ToggleCanvas;
    public GameObject ToggleCanvasAIToday;
    public GameObject ToggleCanvasAIAroundYou;
    public GameObject AIForGoodCanvas;
    public GameObject Panel1Business;
    public GameObject Panel2Business;
    public GameObject Panel3Business;
    public GameObject Panel1AIToday;
    public GameObject Panel2AIToday;
    public GameObject Panel3AIToday;
    public GameObject Panel1AIForGood;
    public GameObject Panel2AIForGood;
    public GameObject Panel3AIForGood;
    public GameObject Panel1AIAroundYou;
    public GameObject Panel2AIAroundYou;
    public GameObject Panel3AIAroundYou;
    public GameObject ScaleToHolder;
    Vector3 originalPos1;
    Vector3 originalPos2;
    Vector3 originalPos3;
    Vector3 originalPos4;
    private Vector3 originalScale1;
    private Vector3 originalScale2;
    private Vector3 originalScale3;
    private Vector3 originalScale4;
    [SerializeField] List<GameObject> lightAnims;
    [SerializeField] GameObject flashAnim;

    // Start is called before the first frame update
    void Start()
    {
        originalPos1 = AIToday.transform.position;
        originalPos2 = AITomorrow.transform.position;
        originalPos3 = AIGood.transform.position;
        originalPos4 = AIAroundYou.transform.position;
        
        originalScale1 = AIToday.transform.localScale;
        originalScale2 = AITomorrow.transform.localScale;
        originalScale3 = AIGood.transform.localScale;
        originalScale4 = AIAroundYou.transform.localScale;
    
        
    }

    private void OnTriggerEnter(Collider other)
    {
        var anim = other.gameObject.GetComponent<ScaleAnimation>();

        if (anim != null)
        {
            StartCoroutine(TaskOnTriggerEnterAnimation(other.gameObject));
        }
        else
        {
            if(other.gameObject.CompareTag("AIToday"))
            {
                toggle1.ThingsHappenWhenSelectAIToday();
                StartCoroutine(ToggleAnimatingCanvasForAiForToday());
                //SceneManager.LoadScene("AIToday");
            }

            if(other.gameObject.CompareTag("AITomorrow"))
            {
                toggle1.ThingsHappenWhenSelectAITomorrow();
                StartCoroutine(ToggleAnimatingCanvasForAiForBusiness());
                //TransitionBusiness.Transition();
                //SceneManager.LoadSceneAsync("AIBusiness");
            
            }

            if (other.gameObject.CompareTag("AIGood"))
            {
                toggle1.ThingsHappenWhenSelectAIGood();
                StartCoroutine(ToggleAnimatingCanvasForAiForGood());
            
                //SceneManager.LoadScene("AIGood");
            }

            if(other.gameObject.CompareTag("AIAroundYou"))
            {
                toggle1.ThingsHappenWhenSelectAIAroundYou();
                StartCoroutine(ToggleAnimatingCanvasForAIAroundYou());
                //SceneManager.LoadScene("AIAroundYou");
            }

            AIToday.transform.position = originalPos1;
            AITomorrow.transform.position = originalPos2;
            AIGood.transform.position = originalPos3;
            AIAroundYou.transform.position = originalPos4;
        
            AIToday.transform.localScale = originalScale1;
            AITomorrow.transform.localScale = originalScale2;
            AIGood.transform.localScale = originalScale3;
            AIAroundYou.transform.localScale = originalScale4;
        }
    }
    
    IEnumerator TaskOnTriggerEnterAnimation(GameObject enterObject)
    {
        var anim = enterObject.GetComponent<ScaleAnimation>();
        
        enterObject.GetComponent<Draggable>().CleanUp();
        
        anim.StartAnimation(ScaleToHolder.transform);

        //flashAnim.GetComponent<Animator>().SetTrigger("Flash");

        lightAnims.ForEach(obj => obj.GetComponent<VolumetricBeamAnimation>().StartAnim());

        while (anim.IsMoving())
        {
            yield return new WaitForEndOfFrame();
        }
        
        if (enterObject.CompareTag("AIToday"))
        {
            toggle1.ThingsHappenWhenSelectAIToday();
            StartCoroutine(ToggleAnimatingCanvasForAiForToday());
            //SceneManager.LoadScene("AIToday");
        }

        if(enterObject.CompareTag("AITomorrow"))
        {
            toggle1.ThingsHappenWhenSelectAITomorrow();
            StartCoroutine(ToggleAnimatingCanvasForAiForBusiness());
            //TransitionBusiness.Transition();
            //SceneManager.LoadSceneAsync("AIBusiness");
            
        }

        if (enterObject.CompareTag("AIGood"))
        {
            toggle1.ThingsHappenWhenSelectAIGood();
            StartCoroutine(ToggleAnimatingCanvasForAiForGood());
            
            //SceneManager.LoadScene("AIGood");
        }

        if(enterObject.CompareTag("AIAroundYou"))
        {
            toggle1.ThingsHappenWhenSelectAIAroundYou();
            StartCoroutine(ToggleAnimatingCanvasForAIAroundYou());
            //SceneManager.LoadScene("AIAroundYou");
        }

        AIToday.transform.position = originalPos1;
        AITomorrow.transform.position = originalPos2;
        AIGood.transform.position = originalPos3;
        AIAroundYou.transform.position = originalPos4;
        
        AIToday.transform.localScale = originalScale1;
        AITomorrow.transform.localScale = originalScale2;
        AIGood.transform.localScale = originalScale3;
        AIAroundYou.transform.localScale = originalScale4;
        
    }

    IEnumerator ToggleAnimatingCanvasForAiForBusiness()
    {
        yield return new WaitForSeconds(5.0f);
        ToggleCanvas.SetActive(false);
        Panel1Business.SetActive(true);
        Panel2Business.SetActive(true);
        Panel3Business.SetActive(true);
    }

    IEnumerator ToggleAnimatingCanvasForAiForToday()
    {
        yield return new WaitForSeconds(5.0f);
        ToggleCanvasAIToday.SetActive(false);
        Panel1AIToday.SetActive(true);
        Panel2AIToday.SetActive(true);
        Panel3AIToday.SetActive(true);
    }
    IEnumerator ToggleAnimatingCanvasForAiForGood()
    {
        yield return new WaitForSeconds(5.0f);
        AIForGoodCanvas.SetActive(false);
        Panel1AIForGood.SetActive(true);
        Panel2AIForGood.SetActive(true);
        Panel3AIForGood.SetActive(true);
    }

    IEnumerator ToggleAnimatingCanvasForAIAroundYou()
    {
        yield return new WaitForSeconds(5.0f);
        ToggleCanvasAIAroundYou.SetActive(false);
        Panel1AIAroundYou.SetActive(true);
        Panel2AIAroundYou.SetActive(true);
        Panel3AIAroundYou.SetActive(true);
    }
}
