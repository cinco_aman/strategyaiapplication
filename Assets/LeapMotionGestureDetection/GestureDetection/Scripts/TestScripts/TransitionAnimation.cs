﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransitionAnimation : MonoBehaviour
{
    public GameObject ToggleTransition;
    public GameObject ActivateQuestion;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TransitionOnChange()
    {
        
        StartCoroutine(TransitionOnChangingScene());
    }

    IEnumerator TransitionOnChangingScene()
    {
        yield return new WaitForSeconds(2.0f);
        ToggleTransition.SetActive(false);
        ActivateQuestion.SetActive(true);
    }
}
