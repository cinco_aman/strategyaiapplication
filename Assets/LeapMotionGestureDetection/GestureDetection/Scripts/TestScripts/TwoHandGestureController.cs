﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TwoHandGestureController : MonoBehaviour
{

    
    public GameObject GestureForMainMenuAIForYou;
    public GameObject GestureForMainMenuAIForYourBusiness;
    public GameObject GestureForMainMenuAIForGood;
    public GameObject GestureForMainMenuAIAroundYou;
    public GameObject GestureForAIForYouQuestion1;
    public GameObject GestureForAIForYouQuestion2;
    public GameObject GestureForAIForYouQuestion3;
    public GameObject GestureForAIForYourBusinessQuestion1;
    public GameObject GestureForAIForYourBusinessQuestion2;
    public GameObject GestureForAIForYourBusinessQuestion3;
    public GameObject GestureForAIForGoodQuestion1;
    public GameObject GestureForAIForGoodQuestion2;
    public GameObject GestureForAIForGoodQuestion3;
    public GameObject GestureForAIAroundYouQuestion1;
    public GameObject GestureForAIAroundYouQuestion2;
    public GameObject GestureForAIAroundYouQuestion3;
    public GameObject MainMenu;
    public GameObject AIForYou;
    public GameObject AIForYourBusiness;
    public GameObject AIForGood;
    public GameObject AIForYouAround;
    public GameObject AIToday;
    public GameObject AIBusiness;
    public GameObject AIGood;
    public GameObject AIAroundYou;
    public GameObject SwipeForQuestion1Video1;
    public GameObject SwipeForQuestion1Video2;
    public GameObject SwipeForQuestion2Video1;
    public GameObject SwipeForQuestion2Video2;
    public GameObject SwipeForQuestion3Video1;
    public GameObject SwipeForQuestion3Video2;
    public GameObject SwipeForQuestion4Video1;
    public GameObject SwipeForQuestion4Video2;
    public GameObject SwipeForQuestion5Video1;
    public GameObject SwipeForQuestion5Video2;
    public GameObject SwipeForQuestion6Video1;
    public GameObject SwipeForQuestion6Video2;
    public GameObject SwipeForQuestion7Video1;
    public GameObject SwipeForQuestion7Video2;
    public GameObject SwipeForQuestion8Video1;
    public GameObject SwipeForQuestion8Video2;
    public GameObject SwipeForQuestion9Video1;
    public GameObject SwipeForQuestion9Video2;
    public GameObject SwipeForQuestion10Video1;
    public GameObject SwipeForQuestion10Video2;
    public GameObject SwipeForQuestion11Video1;
    public GameObject SwipeForQuestion11Video2;
    public GameObject SwipeForQuestion12Video1;
    public GameObject SwipeForQuestion12Video2;
    public GameObject LoopVideo;

    public PlayingVideoToggle VideoToggle;
    

    Vector3 originalPos1;
    Vector3 originalPos2;
    Vector3 originalPos3;
    Vector3 originalPos4;


    // Start is called before the first frame update
    void Start()
    {
        originalPos1 = new Vector3(AIToday.transform.position.x, AIToday.transform.position.y, AIToday.transform.position.z);
        originalPos2 = new Vector3(AIBusiness.transform.position.x, AIBusiness.transform.position.y, AIBusiness.transform.position.z);
        originalPos3 = new Vector3(AIGood.transform.position.x, AIGood.transform.position.y, AIGood.transform.position.z);
        originalPos4 = new Vector3(AIAroundYou.transform.position.x, AIAroundYou.transform.position.y, AIAroundYou.transform.position.z);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DoThingsWhenGestureForMainMenuIsTriggered()
    {
        MainMenu.SetActive(true);
        if (MainMenu.activeSelf)
        {
            AIForYou.SetActive(false);
            AIForYourBusiness.SetActive(false);
            AIForGood.SetActive(false);
            AIForYouAround.SetActive(false);

            GestureForMainMenuAIForYou.SetActive(false);
            GestureForMainMenuAIForYourBusiness.SetActive(false);
            GestureForMainMenuAIForGood.SetActive(false);
            GestureForMainMenuAIAroundYou.SetActive(false);

            GestureForAIForYouQuestion1.SetActive(false);
            GestureForAIForYouQuestion2.SetActive(false);
            GestureForAIForYouQuestion3.SetActive(false);

            GestureForAIForYourBusinessQuestion1.SetActive(false);
            GestureForAIForYourBusinessQuestion2.SetActive(false);
            GestureForAIForYourBusinessQuestion3.SetActive(false);

            GestureForAIForGoodQuestion1.SetActive(false);
            GestureForAIForGoodQuestion2.SetActive(false);
            GestureForAIForGoodQuestion3.SetActive(false);

            GestureForAIAroundYouQuestion1.SetActive(false);
            GestureForAIAroundYouQuestion2.SetActive(false);
            GestureForAIAroundYouQuestion3.SetActive(false);



            SwipeForQuestion1Video1.SetActive(false);
            SwipeForQuestion1Video2.SetActive(false);
            SwipeForQuestion2Video1.SetActive(false);
            SwipeForQuestion2Video2.SetActive(false);
            SwipeForQuestion3Video1.SetActive(false);
            SwipeForQuestion3Video2.SetActive(false);

            SwipeForQuestion4Video1.SetActive(false);
            SwipeForQuestion4Video2.SetActive(false);
            SwipeForQuestion5Video1.SetActive(false);
            SwipeForQuestion5Video2.SetActive(false);
            SwipeForQuestion6Video1.SetActive(false);
            SwipeForQuestion6Video2.SetActive(false);

            SwipeForQuestion7Video1.SetActive(false);
            SwipeForQuestion7Video2.SetActive(false);
            SwipeForQuestion8Video1.SetActive(false);
            SwipeForQuestion8Video2.SetActive(false);
            SwipeForQuestion9Video1.SetActive(false);
            SwipeForQuestion9Video2.SetActive(false);

            SwipeForQuestion10Video1.SetActive(false);
            SwipeForQuestion10Video2.SetActive(false);
            SwipeForQuestion11Video1.SetActive(false);
            SwipeForQuestion11Video2.SetActive(false);
            SwipeForQuestion12Video1.SetActive(false);
            SwipeForQuestion12Video2.SetActive(false);
        }

        AIToday.transform.position = originalPos1;
        AIBusiness.transform.position = originalPos2;
        AIGood.transform.position = originalPos3;
        AIAroundYou.transform.position = originalPos4;

    }

    public void DoThingsWhenGestureForAIForYouIsTriggered()
    {
        MainMenu.SetActive(true);
        if (MainMenu.activeSelf)
        {
            AIForYou.SetActive(false);
            AIForYourBusiness.SetActive(false);
            AIForGood.SetActive(false);
            AIForYouAround.SetActive(false);

            GestureForMainMenuAIForYou.SetActive(false);
            GestureForMainMenuAIForYourBusiness.SetActive(false);
            GestureForMainMenuAIForGood.SetActive(false);
            GestureForMainMenuAIAroundYou.SetActive(false);

            GestureForAIForYouQuestion1.SetActive(false);
            GestureForAIForYouQuestion2.SetActive(false);
            GestureForAIForYouQuestion3.SetActive(false);

            GestureForAIForYourBusinessQuestion1.SetActive(false);
            GestureForAIForYourBusinessQuestion2.SetActive(false);
            GestureForAIForYourBusinessQuestion3.SetActive(false);

            GestureForAIForGoodQuestion1.SetActive(false);
            GestureForAIForGoodQuestion2.SetActive(false);
            GestureForAIForGoodQuestion3.SetActive(false);

            GestureForAIAroundYouQuestion1.SetActive(false);
            GestureForAIAroundYouQuestion2.SetActive(false);
            GestureForAIAroundYouQuestion3.SetActive(false);



            SwipeForQuestion1Video1.SetActive(false);
            SwipeForQuestion1Video2.SetActive(false);
            SwipeForQuestion2Video1.SetActive(false);
            SwipeForQuestion2Video2.SetActive(false);
            SwipeForQuestion3Video1.SetActive(false);
            SwipeForQuestion3Video2.SetActive(false);

            SwipeForQuestion4Video1.SetActive(false);
            SwipeForQuestion4Video2.SetActive(false);
            SwipeForQuestion5Video1.SetActive(false);
            SwipeForQuestion5Video2.SetActive(false);
            SwipeForQuestion6Video1.SetActive(false);
            SwipeForQuestion6Video2.SetActive(false);

            SwipeForQuestion7Video1.SetActive(false);
            SwipeForQuestion7Video2.SetActive(false);
            SwipeForQuestion8Video1.SetActive(false);
            SwipeForQuestion8Video2.SetActive(false);
            SwipeForQuestion9Video1.SetActive(false);
            SwipeForQuestion9Video2.SetActive(false);

            SwipeForQuestion10Video1.SetActive(false);
            SwipeForQuestion10Video2.SetActive(false);
            SwipeForQuestion11Video1.SetActive(false);
            SwipeForQuestion11Video2.SetActive(false);
            SwipeForQuestion12Video1.SetActive(false);
            SwipeForQuestion12Video2.SetActive(false);
        }

        AIToday.transform.position = originalPos1;
        AIBusiness.transform.position = originalPos2;
        AIGood.transform.position = originalPos3;
        AIAroundYou.transform.position = originalPos4;

    }

    public void DoThingsWhenGestureForAIForYourBusinessIsTriggered()
    {
        MainMenu.SetActive(true);
        if (MainMenu.activeSelf)
        {
            AIForYou.SetActive(false);
            AIForYourBusiness.SetActive(false);
            AIForGood.SetActive(false);
            AIForYouAround.SetActive(false);

            GestureForMainMenuAIForYou.SetActive(false);
            GestureForMainMenuAIForYourBusiness.SetActive(false);
            GestureForMainMenuAIForGood.SetActive(false);
            GestureForMainMenuAIAroundYou.SetActive(false);

            GestureForAIForYouQuestion1.SetActive(false);
            GestureForAIForYouQuestion2.SetActive(false);
            GestureForAIForYouQuestion3.SetActive(false);

            GestureForAIForYourBusinessQuestion1.SetActive(false);
            GestureForAIForYourBusinessQuestion2.SetActive(false);
            GestureForAIForYourBusinessQuestion3.SetActive(false);

            GestureForAIForGoodQuestion1.SetActive(false);
            GestureForAIForGoodQuestion2.SetActive(false);
            GestureForAIForGoodQuestion3.SetActive(false);

            GestureForAIAroundYouQuestion1.SetActive(false);
            GestureForAIAroundYouQuestion2.SetActive(false);
            GestureForAIAroundYouQuestion3.SetActive(false);



            SwipeForQuestion1Video1.SetActive(false);
            SwipeForQuestion1Video2.SetActive(false);
            SwipeForQuestion2Video1.SetActive(false);
            SwipeForQuestion2Video2.SetActive(false);
            SwipeForQuestion3Video1.SetActive(false);
            SwipeForQuestion3Video2.SetActive(false);

            SwipeForQuestion4Video1.SetActive(false);
            SwipeForQuestion4Video2.SetActive(false);
            SwipeForQuestion5Video1.SetActive(false);
            SwipeForQuestion5Video2.SetActive(false);
            SwipeForQuestion6Video1.SetActive(false);
            SwipeForQuestion6Video2.SetActive(false);

            SwipeForQuestion7Video1.SetActive(false);
            SwipeForQuestion7Video2.SetActive(false);
            SwipeForQuestion8Video1.SetActive(false);
            SwipeForQuestion8Video2.SetActive(false);
            SwipeForQuestion9Video1.SetActive(false);
            SwipeForQuestion9Video2.SetActive(false);

            SwipeForQuestion10Video1.SetActive(false);
            SwipeForQuestion10Video2.SetActive(false);
            SwipeForQuestion11Video1.SetActive(false);
            SwipeForQuestion11Video2.SetActive(false);
            SwipeForQuestion12Video1.SetActive(false);
            SwipeForQuestion12Video2.SetActive(false);
        }

        AIToday.transform.position = originalPos1;
        AIBusiness.transform.position = originalPos2;
        AIGood.transform.position = originalPos3;
        AIAroundYou.transform.position = originalPos4;

    }

    public void DOThingsWhenGestureForAIForGoodIsTriggered()
    {
        MainMenu.SetActive(true);
        if (MainMenu.activeSelf)
        {
            AIForYou.SetActive(false);
            AIForYourBusiness.SetActive(false);
            AIForGood.SetActive(false);
            AIForYouAround.SetActive(false);

            GestureForMainMenuAIForYou.SetActive(false);
            GestureForMainMenuAIForYourBusiness.SetActive(false);
            GestureForMainMenuAIForGood.SetActive(false);
            GestureForMainMenuAIAroundYou.SetActive(false);

            GestureForAIForYouQuestion1.SetActive(false);
            GestureForAIForYouQuestion2.SetActive(false);
            GestureForAIForYouQuestion3.SetActive(false);

            GestureForAIForYourBusinessQuestion1.SetActive(false);
            GestureForAIForYourBusinessQuestion2.SetActive(false);
            GestureForAIForYourBusinessQuestion3.SetActive(false);

            GestureForAIForGoodQuestion1.SetActive(false);
            GestureForAIForGoodQuestion2.SetActive(false);
            GestureForAIForGoodQuestion3.SetActive(false);

            GestureForAIAroundYouQuestion1.SetActive(false);
            GestureForAIAroundYouQuestion2.SetActive(false);
            GestureForAIAroundYouQuestion3.SetActive(false);



            SwipeForQuestion1Video1.SetActive(false);
            SwipeForQuestion1Video2.SetActive(false);
            SwipeForQuestion2Video1.SetActive(false);
            SwipeForQuestion2Video2.SetActive(false);
            SwipeForQuestion3Video1.SetActive(false);
            SwipeForQuestion3Video2.SetActive(false);

            SwipeForQuestion4Video1.SetActive(false);
            SwipeForQuestion4Video2.SetActive(false);
            SwipeForQuestion5Video1.SetActive(false);
            SwipeForQuestion5Video2.SetActive(false);
            SwipeForQuestion6Video1.SetActive(false);
            SwipeForQuestion6Video2.SetActive(false);

            SwipeForQuestion7Video1.SetActive(false);
            SwipeForQuestion7Video2.SetActive(false);
            SwipeForQuestion8Video1.SetActive(false);
            SwipeForQuestion8Video2.SetActive(false);
            SwipeForQuestion9Video1.SetActive(false);
            SwipeForQuestion9Video2.SetActive(false);

            SwipeForQuestion10Video1.SetActive(false);
            SwipeForQuestion10Video2.SetActive(false);
            SwipeForQuestion11Video1.SetActive(false);
            SwipeForQuestion11Video2.SetActive(false);
            SwipeForQuestion12Video1.SetActive(false);
            SwipeForQuestion12Video2.SetActive(false);
        }

        AIToday.transform.position = originalPos1;
        AIBusiness.transform.position = originalPos2;
        AIGood.transform.position = originalPos3;
        AIAroundYou.transform.position = originalPos4;


    }

    public void DOThingsWhenGestureForAIAroundYouIsTriggered()
    {
        MainMenu.SetActive(true);
        if (MainMenu.activeSelf)
        {
            AIForYou.SetActive(false);
            AIForYourBusiness.SetActive(false);
            AIForGood.SetActive(false);
            AIForYouAround.SetActive(false);

            GestureForMainMenuAIForYou.SetActive(false);
            GestureForMainMenuAIForYourBusiness.SetActive(false);
            GestureForMainMenuAIForGood.SetActive(false);
            GestureForMainMenuAIAroundYou.SetActive(false);

            GestureForAIForYouQuestion1.SetActive(false);
            GestureForAIForYouQuestion2.SetActive(false);
            GestureForAIForYouQuestion3.SetActive(false);

            GestureForAIForYourBusinessQuestion1.SetActive(false);
            GestureForAIForYourBusinessQuestion2.SetActive(false);
            GestureForAIForYourBusinessQuestion3.SetActive(false);

            GestureForAIForGoodQuestion1.SetActive(false);
            GestureForAIForGoodQuestion2.SetActive(false);
            GestureForAIForGoodQuestion3.SetActive(false);

            GestureForAIAroundYouQuestion1.SetActive(false);
            GestureForAIAroundYouQuestion2.SetActive(false);
            GestureForAIAroundYouQuestion3.SetActive(false);



            SwipeForQuestion1Video1.SetActive(false);
            SwipeForQuestion1Video2.SetActive(false);
            SwipeForQuestion2Video1.SetActive(false);
            SwipeForQuestion2Video2.SetActive(false);
            SwipeForQuestion3Video1.SetActive(false);
            SwipeForQuestion3Video2.SetActive(false);

            SwipeForQuestion4Video1.SetActive(false);
            SwipeForQuestion4Video2.SetActive(false);
            SwipeForQuestion5Video1.SetActive(false);
            SwipeForQuestion5Video2.SetActive(false);
            SwipeForQuestion6Video1.SetActive(false);
            SwipeForQuestion6Video2.SetActive(false);

            SwipeForQuestion7Video1.SetActive(false);
            SwipeForQuestion7Video2.SetActive(false);
            SwipeForQuestion8Video1.SetActive(false);
            SwipeForQuestion8Video2.SetActive(false);
            SwipeForQuestion9Video1.SetActive(false);
            SwipeForQuestion9Video2.SetActive(false);

            SwipeForQuestion10Video1.SetActive(false);
            SwipeForQuestion10Video2.SetActive(false);
            SwipeForQuestion11Video1.SetActive(false);
            SwipeForQuestion11Video2.SetActive(false);
            SwipeForQuestion12Video1.SetActive(false);
            SwipeForQuestion12Video2.SetActive(false);
        }

        AIToday.transform.position = originalPos1;
        AIBusiness.transform.position = originalPos2;
        AIGood.transform.position = originalPos3;
        AIAroundYou.transform.position = originalPos4;


    }

    public void DoThingsWhenGestureForAIForYouQuestion1IsTriggered()
    {
        AIForYou.SetActive(true);
        if (AIForYou.activeSelf)
        {
            AIForYourBusiness.SetActive(false);
            AIForGood.SetActive(false);
            AIForYouAround.SetActive(false);

            MainMenu.SetActive(false);

            StartCoroutine(WaitForActivatingGestureForMainMenuAIForYou());
            GestureForMainMenuAIForYourBusiness.SetActive(false);
            GestureForMainMenuAIForGood.SetActive(false);
            GestureForMainMenuAIAroundYou.SetActive(false);

            GestureForAIForYouQuestion1.SetActive(false);
            GestureForAIForYouQuestion2.SetActive(false);
            GestureForAIForYouQuestion3.SetActive(false);

            GestureForAIForYourBusinessQuestion1.SetActive(false);
            GestureForAIForYourBusinessQuestion2.SetActive(false);
            GestureForAIForYourBusinessQuestion3.SetActive(false);

            GestureForAIForGoodQuestion1.SetActive(false);
            GestureForAIForGoodQuestion2.SetActive(false);
            GestureForAIForGoodQuestion3.SetActive(false);

            GestureForAIAroundYouQuestion1.SetActive(false);
            GestureForAIAroundYouQuestion2.SetActive(false);
            GestureForAIAroundYouQuestion3.SetActive(false);



            SwipeForQuestion1Video1.SetActive(false);
            SwipeForQuestion1Video2.SetActive(false);
            SwipeForQuestion2Video1.SetActive(false);
            SwipeForQuestion2Video2.SetActive(false);
            SwipeForQuestion3Video1.SetActive(false);
            SwipeForQuestion3Video2.SetActive(false);

            SwipeForQuestion4Video1.SetActive(false);
            SwipeForQuestion4Video2.SetActive(false);
            SwipeForQuestion5Video1.SetActive(false);
            SwipeForQuestion5Video2.SetActive(false);
            SwipeForQuestion6Video1.SetActive(false);
            SwipeForQuestion6Video2.SetActive(false);

            SwipeForQuestion7Video1.SetActive(false);
            SwipeForQuestion7Video2.SetActive(false);
            SwipeForQuestion8Video1.SetActive(false);
            SwipeForQuestion8Video2.SetActive(false);
            SwipeForQuestion9Video1.SetActive(false);
            SwipeForQuestion9Video2.SetActive(false);

            SwipeForQuestion10Video1.SetActive(false);
            SwipeForQuestion10Video2.SetActive(false);
            SwipeForQuestion11Video1.SetActive(false);
            SwipeForQuestion11Video2.SetActive(false);
            SwipeForQuestion12Video1.SetActive(false);
            SwipeForQuestion12Video2.SetActive(false);

            VideoToggle.Video1.SetActive(false);
            VideoToggle.Video2.SetActive(false);
            VideoToggle.Video3.SetActive(false);
            VideoToggle.Video4.SetActive(false);
            VideoToggle.Video5.SetActive(false);
            VideoToggle.Video6.SetActive(false);
            VideoToggle.Video7.SetActive(false);
            VideoToggle.Video8.SetActive(false);
            VideoToggle.Video9.SetActive(false);
            VideoToggle.Video10.SetActive(false);
            VideoToggle.Video11.SetActive(false);
            VideoToggle.Video12.SetActive(false);
            VideoToggle.Video1.SetActive(false);
            VideoToggle.Video14.SetActive(false);
            VideoToggle.Video15.SetActive(false);
            VideoToggle.Video16.SetActive(false);
            VideoToggle.Video17.SetActive(false);
            VideoToggle.Video18.SetActive(false);
            VideoToggle.Video19.SetActive(false);
            VideoToggle.Video20.SetActive(false);
            VideoToggle.Video21.SetActive(false);
            VideoToggle.Video22.SetActive(false);
            VideoToggle.Video23.SetActive(false);
            VideoToggle.Video24.SetActive(false);
            VideoToggle.IntroVideo.SetActive(true);
        }
    }


    public void DoThingsWhenGestureForAIForYouQuestion2IsTriggered()
    {
        AIForYou.SetActive(true);
        if (AIForYou.activeSelf)
        {
            AIForYourBusiness.SetActive(false);
            AIForGood.SetActive(false);
            AIForYouAround.SetActive(false);

            MainMenu.SetActive(false);

            StartCoroutine(WaitForActivatingGestureForMainMenuAIForYou());

            GestureForMainMenuAIForYourBusiness.SetActive(false);
            GestureForMainMenuAIForGood.SetActive(false);
            GestureForMainMenuAIAroundYou.SetActive(false);

            GestureForAIForYouQuestion1.SetActive(false);
            GestureForAIForYouQuestion2.SetActive(false);
            GestureForAIForYouQuestion3.SetActive(false);

            GestureForAIForYourBusinessQuestion1.SetActive(false);
            GestureForAIForYourBusinessQuestion2.SetActive(false);
            GestureForAIForYourBusinessQuestion3.SetActive(false);

            GestureForAIForGoodQuestion1.SetActive(false);
            GestureForAIForGoodQuestion2.SetActive(false);
            GestureForAIForGoodQuestion3.SetActive(false);

            GestureForAIAroundYouQuestion1.SetActive(false);
            GestureForAIAroundYouQuestion2.SetActive(false);
            GestureForAIAroundYouQuestion3.SetActive(false);



            SwipeForQuestion1Video1.SetActive(false);
            SwipeForQuestion1Video2.SetActive(false);
            SwipeForQuestion2Video1.SetActive(false);
            SwipeForQuestion2Video2.SetActive(false);
            SwipeForQuestion3Video1.SetActive(false);
            SwipeForQuestion3Video2.SetActive(false);

            SwipeForQuestion4Video1.SetActive(false);
            SwipeForQuestion4Video2.SetActive(false);
            SwipeForQuestion5Video1.SetActive(false);
            SwipeForQuestion5Video2.SetActive(false);
            SwipeForQuestion6Video1.SetActive(false);
            SwipeForQuestion6Video2.SetActive(false);

            SwipeForQuestion7Video1.SetActive(false);
            SwipeForQuestion7Video2.SetActive(false);
            SwipeForQuestion8Video1.SetActive(false);
            SwipeForQuestion8Video2.SetActive(false);
            SwipeForQuestion9Video1.SetActive(false);
            SwipeForQuestion9Video2.SetActive(false);

            SwipeForQuestion10Video1.SetActive(false);
            SwipeForQuestion10Video2.SetActive(false);
            SwipeForQuestion11Video1.SetActive(false);
            SwipeForQuestion11Video2.SetActive(false);
            SwipeForQuestion12Video1.SetActive(false);
            SwipeForQuestion12Video2.SetActive(false);


            VideoToggle.Video1.SetActive(false);
            VideoToggle.Video2.SetActive(false);
            VideoToggle.Video3.SetActive(false);
            VideoToggle.Video4.SetActive(false);
            VideoToggle.Video5.SetActive(false);
            VideoToggle.Video6.SetActive(false);
            VideoToggle.Video7.SetActive(false);
            VideoToggle.Video8.SetActive(false);
            VideoToggle.Video9.SetActive(false);
            VideoToggle.Video10.SetActive(false);
            VideoToggle.Video11.SetActive(false);
            VideoToggle.Video12.SetActive(false);
            VideoToggle.Video1.SetActive(false);
            VideoToggle.Video14.SetActive(false);
            VideoToggle.Video15.SetActive(false);
            VideoToggle.Video16.SetActive(false);
            VideoToggle.Video17.SetActive(false);
            VideoToggle.Video18.SetActive(false);
            VideoToggle.Video19.SetActive(false);
            VideoToggle.Video20.SetActive(false);
            VideoToggle.Video21.SetActive(false);
            VideoToggle.Video22.SetActive(false);
            VideoToggle.Video23.SetActive(false);
            VideoToggle.Video24.SetActive(false);
            VideoToggle.IntroVideo.SetActive(true);
        }
    }


    public void DoThingsWhenGestureForAIForYouQuestion3IsTriggered()
    {
        AIForYou.SetActive(true);
        if (AIForYou.activeSelf)
        {
            AIForYourBusiness.SetActive(false);
            AIForGood.SetActive(false);
            AIForYouAround.SetActive(false);

            MainMenu.SetActive(false);

            StartCoroutine(WaitForActivatingGestureForMainMenuAIForYou());

            GestureForMainMenuAIForYourBusiness.SetActive(false);
            GestureForMainMenuAIForGood.SetActive(false);
            GestureForMainMenuAIAroundYou.SetActive(false);

            GestureForAIForYouQuestion1.SetActive(false);
            GestureForAIForYouQuestion2.SetActive(false);
            GestureForAIForYouQuestion3.SetActive(false);

            GestureForAIForYourBusinessQuestion1.SetActive(false);
            GestureForAIForYourBusinessQuestion2.SetActive(false);
            GestureForAIForYourBusinessQuestion3.SetActive(false);

            GestureForAIForGoodQuestion1.SetActive(false);
            GestureForAIForGoodQuestion2.SetActive(false);
            GestureForAIForGoodQuestion3.SetActive(false);

            GestureForAIAroundYouQuestion1.SetActive(false);
            GestureForAIAroundYouQuestion2.SetActive(false);
            GestureForAIAroundYouQuestion3.SetActive(false);



            SwipeForQuestion1Video1.SetActive(false);
            SwipeForQuestion1Video2.SetActive(false);
            SwipeForQuestion2Video1.SetActive(false);
            SwipeForQuestion2Video2.SetActive(false);
            SwipeForQuestion3Video1.SetActive(false);
            SwipeForQuestion3Video2.SetActive(false);

            SwipeForQuestion4Video1.SetActive(false);
            SwipeForQuestion4Video2.SetActive(false);
            SwipeForQuestion5Video1.SetActive(false);
            SwipeForQuestion5Video2.SetActive(false);
            SwipeForQuestion6Video1.SetActive(false);
            SwipeForQuestion6Video2.SetActive(false);

            SwipeForQuestion7Video1.SetActive(false);
            SwipeForQuestion7Video2.SetActive(false);
            SwipeForQuestion8Video1.SetActive(false);
            SwipeForQuestion8Video2.SetActive(false);
            SwipeForQuestion9Video1.SetActive(false);
            SwipeForQuestion9Video2.SetActive(false);

            SwipeForQuestion10Video1.SetActive(false);
            SwipeForQuestion10Video2.SetActive(false);
            SwipeForQuestion11Video1.SetActive(false);
            SwipeForQuestion11Video2.SetActive(false);
            SwipeForQuestion12Video1.SetActive(false);
            SwipeForQuestion12Video2.SetActive(false);

            VideoToggle.Video1.SetActive(false);
            VideoToggle.Video2.SetActive(false);
            VideoToggle.Video3.SetActive(false);
            VideoToggle.Video4.SetActive(false);
            VideoToggle.Video5.SetActive(false);
            VideoToggle.Video6.SetActive(false);
            VideoToggle.Video7.SetActive(false);
            VideoToggle.Video8.SetActive(false);
            VideoToggle.Video9.SetActive(false);
            VideoToggle.Video10.SetActive(false);
            VideoToggle.Video11.SetActive(false);
            VideoToggle.Video12.SetActive(false);
            VideoToggle.Video1.SetActive(false);
            VideoToggle.Video14.SetActive(false);
            VideoToggle.Video15.SetActive(false);
            VideoToggle.Video16.SetActive(false);
            VideoToggle.Video17.SetActive(false);
            VideoToggle.Video18.SetActive(false);
            VideoToggle.Video19.SetActive(false);
            VideoToggle.Video20.SetActive(false);
            VideoToggle.Video21.SetActive(false);
            VideoToggle.Video22.SetActive(false);
            VideoToggle.Video23.SetActive(false);
            VideoToggle.Video24.SetActive(false);
            VideoToggle.IntroVideo.SetActive(true);

        }
    }

    public void DoThingsWhenGestureForAIForYourBusinessQuestion1IsTriggered()
    {
        AIForYourBusiness.SetActive(true);
        if (AIForYourBusiness.activeSelf)
        {
            AIForYou.SetActive(false);
            AIForGood.SetActive(false);
            AIForYouAround.SetActive(false);

            MainMenu.SetActive(false);

            StartCoroutine(WaitForActivatingGestureForMainMenuAIForYourBusiness());

            GestureForMainMenuAIForYou.SetActive(false);
            GestureForMainMenuAIForGood.SetActive(false);
            GestureForMainMenuAIAroundYou.SetActive(false);

            GestureForAIForYouQuestion1.SetActive(false);
            GestureForAIForYouQuestion2.SetActive(false);
            GestureForAIForYouQuestion3.SetActive(false);

            GestureForAIForYourBusinessQuestion1.SetActive(false);
            GestureForAIForYourBusinessQuestion2.SetActive(false);
            GestureForAIForYourBusinessQuestion3.SetActive(false);

            GestureForAIForGoodQuestion1.SetActive(false);
            GestureForAIForGoodQuestion2.SetActive(false);
            GestureForAIForGoodQuestion3.SetActive(false);

            GestureForAIAroundYouQuestion1.SetActive(false);
            GestureForAIAroundYouQuestion2.SetActive(false);
            GestureForAIAroundYouQuestion3.SetActive(false);



            SwipeForQuestion1Video1.SetActive(false);
            SwipeForQuestion1Video2.SetActive(false);
            SwipeForQuestion2Video1.SetActive(false);
            SwipeForQuestion2Video2.SetActive(false);
            SwipeForQuestion3Video1.SetActive(false);
            SwipeForQuestion3Video2.SetActive(false);

            SwipeForQuestion4Video1.SetActive(false);
            SwipeForQuestion4Video2.SetActive(false);
            SwipeForQuestion5Video1.SetActive(false);
            SwipeForQuestion5Video2.SetActive(false);
            SwipeForQuestion6Video1.SetActive(false);
            SwipeForQuestion6Video2.SetActive(false);

            SwipeForQuestion7Video1.SetActive(false);
            SwipeForQuestion7Video2.SetActive(false);
            SwipeForQuestion8Video1.SetActive(false);
            SwipeForQuestion8Video2.SetActive(false);
            SwipeForQuestion9Video1.SetActive(false);
            SwipeForQuestion9Video2.SetActive(false);

            SwipeForQuestion10Video1.SetActive(false);
            SwipeForQuestion10Video2.SetActive(false);
            SwipeForQuestion11Video1.SetActive(false);
            SwipeForQuestion11Video2.SetActive(false);
            SwipeForQuestion12Video1.SetActive(false);
            SwipeForQuestion12Video2.SetActive(false);


            VideoToggle.Video1.SetActive(false);
            VideoToggle.Video2.SetActive(false);
            VideoToggle.Video3.SetActive(false);
            VideoToggle.Video4.SetActive(false);
            VideoToggle.Video5.SetActive(false);
            VideoToggle.Video6.SetActive(false);
            VideoToggle.Video7.SetActive(false);
            VideoToggle.Video8.SetActive(false);
            VideoToggle.Video9.SetActive(false);
            VideoToggle.Video10.SetActive(false);
            VideoToggle.Video11.SetActive(false);
            VideoToggle.Video12.SetActive(false);
            VideoToggle.Video1.SetActive(false);
            VideoToggle.Video14.SetActive(false);
            VideoToggle.Video15.SetActive(false);
            VideoToggle.Video16.SetActive(false);
            VideoToggle.Video17.SetActive(false);
            VideoToggle.Video18.SetActive(false);
            VideoToggle.Video19.SetActive(false);
            VideoToggle.Video20.SetActive(false);
            VideoToggle.Video21.SetActive(false);
            VideoToggle.Video22.SetActive(false);
            VideoToggle.Video23.SetActive(false);
            VideoToggle.Video24.SetActive(false);
            VideoToggle.IntroVideo.SetActive(true);

        }
    }

    public void DoThingsWhenGestureForAIForYourBusinessQuestion2IsTriggered()
    {
        AIForYourBusiness.SetActive(true);
        if (AIForYourBusiness.activeSelf)
        {
            AIForYou.SetActive(false);
            AIForGood.SetActive(false);
            AIForYouAround.SetActive(false);

            MainMenu.SetActive(false);

            StartCoroutine(WaitForActivatingGestureForMainMenuAIForYourBusiness());

            GestureForMainMenuAIForYou.SetActive(false);
            GestureForMainMenuAIForGood.SetActive(false);
            GestureForMainMenuAIAroundYou.SetActive(false);

            GestureForAIForYouQuestion1.SetActive(false);
            GestureForAIForYouQuestion2.SetActive(false);
            GestureForAIForYouQuestion3.SetActive(false);

            GestureForAIForYourBusinessQuestion1.SetActive(false);
            GestureForAIForYourBusinessQuestion2.SetActive(false);
            GestureForAIForYourBusinessQuestion3.SetActive(false);

            GestureForAIForGoodQuestion1.SetActive(false);
            GestureForAIForGoodQuestion2.SetActive(false);
            GestureForAIForGoodQuestion3.SetActive(false);

            GestureForAIAroundYouQuestion1.SetActive(false);
            GestureForAIAroundYouQuestion2.SetActive(false);
            GestureForAIAroundYouQuestion3.SetActive(false);



            SwipeForQuestion1Video1.SetActive(false);
            SwipeForQuestion1Video2.SetActive(false);
            SwipeForQuestion2Video1.SetActive(false);
            SwipeForQuestion2Video2.SetActive(false);
            SwipeForQuestion3Video1.SetActive(false);
            SwipeForQuestion3Video2.SetActive(false);

            SwipeForQuestion4Video1.SetActive(false);
            SwipeForQuestion4Video2.SetActive(false);
            SwipeForQuestion5Video1.SetActive(false);
            SwipeForQuestion5Video2.SetActive(false);
            SwipeForQuestion6Video1.SetActive(false);
            SwipeForQuestion6Video2.SetActive(false);

            SwipeForQuestion7Video1.SetActive(false);
            SwipeForQuestion7Video2.SetActive(false);
            SwipeForQuestion8Video1.SetActive(false);
            SwipeForQuestion8Video2.SetActive(false);
            SwipeForQuestion9Video1.SetActive(false);
            SwipeForQuestion9Video2.SetActive(false);

            SwipeForQuestion10Video1.SetActive(false);
            SwipeForQuestion10Video2.SetActive(false);
            SwipeForQuestion11Video1.SetActive(false);
            SwipeForQuestion11Video2.SetActive(false);
            SwipeForQuestion12Video1.SetActive(false);
            SwipeForQuestion12Video2.SetActive(false);


            VideoToggle.Video1.SetActive(false);
            VideoToggle.Video2.SetActive(false);
            VideoToggle.Video3.SetActive(false);
            VideoToggle.Video4.SetActive(false);
            VideoToggle.Video5.SetActive(false);
            VideoToggle.Video6.SetActive(false);
            VideoToggle.Video7.SetActive(false);
            VideoToggle.Video8.SetActive(false);
            VideoToggle.Video9.SetActive(false);
            VideoToggle.Video10.SetActive(false);
            VideoToggle.Video11.SetActive(false);
            VideoToggle.Video12.SetActive(false);
            VideoToggle.Video1.SetActive(false);
            VideoToggle.Video14.SetActive(false);
            VideoToggle.Video15.SetActive(false);
            VideoToggle.Video16.SetActive(false);
            VideoToggle.Video17.SetActive(false);
            VideoToggle.Video18.SetActive(false);
            VideoToggle.Video19.SetActive(false);
            VideoToggle.Video20.SetActive(false);
            VideoToggle.Video21.SetActive(false);
            VideoToggle.Video22.SetActive(false);
            VideoToggle.Video23.SetActive(false);
            VideoToggle.Video24.SetActive(false);
            VideoToggle.IntroVideo.SetActive(true);

        }
    }

    public void DoThingsWhenGestureForAIForYourBusinessQuestion3IsTriggered()
    {
        AIForYourBusiness.SetActive(true);
        if (AIForYourBusiness.activeSelf)
        {
            AIForYou.SetActive(false);
            AIForGood.SetActive(false);
            AIForYouAround.SetActive(false);

            MainMenu.SetActive(false);

            StartCoroutine(WaitForActivatingGestureForMainMenuAIForYourBusiness());

            GestureForMainMenuAIForYou.SetActive(false);
            GestureForMainMenuAIForGood.SetActive(false);
            GestureForMainMenuAIAroundYou.SetActive(false);

            GestureForAIForYouQuestion1.SetActive(false);
            GestureForAIForYouQuestion2.SetActive(false);
            GestureForAIForYouQuestion3.SetActive(false);

            GestureForAIForYourBusinessQuestion1.SetActive(false);
            GestureForAIForYourBusinessQuestion2.SetActive(false);
            GestureForAIForYourBusinessQuestion3.SetActive(false);

            GestureForAIForGoodQuestion1.SetActive(false);
            GestureForAIForGoodQuestion2.SetActive(false);
            GestureForAIForGoodQuestion3.SetActive(false);

            GestureForAIAroundYouQuestion1.SetActive(false);
            GestureForAIAroundYouQuestion2.SetActive(false);
            GestureForAIAroundYouQuestion3.SetActive(false);



            SwipeForQuestion1Video1.SetActive(false);
            SwipeForQuestion1Video2.SetActive(false);
            SwipeForQuestion2Video1.SetActive(false);
            SwipeForQuestion2Video2.SetActive(false);
            SwipeForQuestion3Video1.SetActive(false);
            SwipeForQuestion3Video2.SetActive(false);

            SwipeForQuestion4Video1.SetActive(false);
            SwipeForQuestion4Video2.SetActive(false);
            SwipeForQuestion5Video1.SetActive(false);
            SwipeForQuestion5Video2.SetActive(false);
            SwipeForQuestion6Video1.SetActive(false);
            SwipeForQuestion6Video2.SetActive(false);

            SwipeForQuestion7Video1.SetActive(false);
            SwipeForQuestion7Video2.SetActive(false);
            SwipeForQuestion8Video1.SetActive(false);
            SwipeForQuestion8Video2.SetActive(false);
            SwipeForQuestion9Video1.SetActive(false);
            SwipeForQuestion9Video2.SetActive(false);

            SwipeForQuestion10Video1.SetActive(false);
            SwipeForQuestion10Video2.SetActive(false);
            SwipeForQuestion11Video1.SetActive(false);
            SwipeForQuestion11Video2.SetActive(false);
            SwipeForQuestion12Video1.SetActive(false);
            SwipeForQuestion12Video2.SetActive(false);


            VideoToggle.Video1.SetActive(false);
            VideoToggle.Video2.SetActive(false);
            VideoToggle.Video3.SetActive(false);
            VideoToggle.Video4.SetActive(false);
            VideoToggle.Video5.SetActive(false);
            VideoToggle.Video6.SetActive(false);
            VideoToggle.Video7.SetActive(false);
            VideoToggle.Video8.SetActive(false);
            VideoToggle.Video9.SetActive(false);
            VideoToggle.Video10.SetActive(false);
            VideoToggle.Video11.SetActive(false);
            VideoToggle.Video12.SetActive(false);
            VideoToggle.Video1.SetActive(false);
            VideoToggle.Video14.SetActive(false);
            VideoToggle.Video15.SetActive(false);
            VideoToggle.Video16.SetActive(false);
            VideoToggle.Video17.SetActive(false);
            VideoToggle.Video18.SetActive(false);
            VideoToggle.Video19.SetActive(false);
            VideoToggle.Video20.SetActive(false);
            VideoToggle.Video21.SetActive(false);
            VideoToggle.Video22.SetActive(false);
            VideoToggle.Video23.SetActive(false);
            VideoToggle.Video24.SetActive(false);
            VideoToggle.IntroVideo.SetActive(true);

        }
    }

    public void DoThingsWhenGestureForAIForGoodQuestion1IsTriggered()
    {
        AIForGood.SetActive(true);
        if (AIForGood.activeSelf)
        {
            AIForYou.SetActive(false);
            AIForYourBusiness.SetActive(false);
            AIForYouAround.SetActive(false);

            MainMenu.SetActive(false);

            StartCoroutine(WaitForActivatingGestureForMainMenuAIForGood());

            GestureForMainMenuAIForYou.SetActive(false);
            GestureForMainMenuAIForGood.SetActive(false);
            GestureForMainMenuAIAroundYou.SetActive(false);

            GestureForAIForYouQuestion1.SetActive(false);
            GestureForAIForYouQuestion2.SetActive(false);
            GestureForAIForYouQuestion3.SetActive(false);

            GestureForAIForYourBusinessQuestion1.SetActive(false);
            GestureForAIForYourBusinessQuestion2.SetActive(false);
            GestureForAIForYourBusinessQuestion3.SetActive(false);

            GestureForAIForGoodQuestion1.SetActive(false);
            GestureForAIForGoodQuestion2.SetActive(false);
            GestureForAIForGoodQuestion3.SetActive(false);

            GestureForAIAroundYouQuestion1.SetActive(false);
            GestureForAIAroundYouQuestion2.SetActive(false);
            GestureForAIAroundYouQuestion3.SetActive(false);



            SwipeForQuestion1Video1.SetActive(false);
            SwipeForQuestion1Video2.SetActive(false);
            SwipeForQuestion2Video1.SetActive(false);
            SwipeForQuestion2Video2.SetActive(false);
            SwipeForQuestion3Video1.SetActive(false);
            SwipeForQuestion3Video2.SetActive(false);

            SwipeForQuestion4Video1.SetActive(false);
            SwipeForQuestion4Video2.SetActive(false);
            SwipeForQuestion5Video1.SetActive(false);
            SwipeForQuestion5Video2.SetActive(false);
            SwipeForQuestion6Video1.SetActive(false);
            SwipeForQuestion6Video2.SetActive(false);

            SwipeForQuestion7Video1.SetActive(false);
            SwipeForQuestion7Video2.SetActive(false);
            SwipeForQuestion8Video1.SetActive(false);
            SwipeForQuestion8Video2.SetActive(false);
            SwipeForQuestion9Video1.SetActive(false);
            SwipeForQuestion9Video2.SetActive(false);

            SwipeForQuestion10Video1.SetActive(false);
            SwipeForQuestion10Video2.SetActive(false);
            SwipeForQuestion11Video1.SetActive(false);
            SwipeForQuestion11Video2.SetActive(false);
            SwipeForQuestion12Video1.SetActive(false);
            SwipeForQuestion12Video2.SetActive(false);


            VideoToggle.Video1.SetActive(false);
            VideoToggle.Video2.SetActive(false);
            VideoToggle.Video3.SetActive(false);
            VideoToggle.Video4.SetActive(false);
            VideoToggle.Video5.SetActive(false);
            VideoToggle.Video6.SetActive(false);
            VideoToggle.Video7.SetActive(false);
            VideoToggle.Video8.SetActive(false);
            VideoToggle.Video9.SetActive(false);
            VideoToggle.Video10.SetActive(false);
            VideoToggle.Video11.SetActive(false);
            VideoToggle.Video12.SetActive(false);
            VideoToggle.Video1.SetActive(false);
            VideoToggle.Video14.SetActive(false);
            VideoToggle.Video15.SetActive(false);
            VideoToggle.Video16.SetActive(false);
            VideoToggle.Video17.SetActive(false);
            VideoToggle.Video18.SetActive(false);
            VideoToggle.Video19.SetActive(false);
            VideoToggle.Video20.SetActive(false);
            VideoToggle.Video21.SetActive(false);
            VideoToggle.Video22.SetActive(false);
            VideoToggle.Video23.SetActive(false);
            VideoToggle.Video24.SetActive(false);
            VideoToggle.IntroVideo.SetActive(true);

        }
    }

    public void DoThingsWhenGestureForAIForGoodQuestion2IsTriggered()
    {
        AIForGood.SetActive(true);
        if (AIForGood.activeSelf)
        {
            AIForYou.SetActive(false);
            AIForYourBusiness.SetActive(false);
            AIForYouAround.SetActive(false);

            MainMenu.SetActive(false);

            StartCoroutine(WaitForActivatingGestureForMainMenuAIForGood());

            GestureForMainMenuAIForYou.SetActive(false);
            GestureForMainMenuAIForGood.SetActive(false);
            GestureForMainMenuAIAroundYou.SetActive(false);

            GestureForAIForYouQuestion1.SetActive(false);
            GestureForAIForYouQuestion2.SetActive(false);
            GestureForAIForYouQuestion3.SetActive(false);

            GestureForAIForYourBusinessQuestion1.SetActive(false);
            GestureForAIForYourBusinessQuestion2.SetActive(false);
            GestureForAIForYourBusinessQuestion3.SetActive(false);

            GestureForAIForGoodQuestion1.SetActive(false);
            GestureForAIForGoodQuestion2.SetActive(false);
            GestureForAIForGoodQuestion3.SetActive(false);

            GestureForAIAroundYouQuestion1.SetActive(false);
            GestureForAIAroundYouQuestion2.SetActive(false);
            GestureForAIAroundYouQuestion3.SetActive(false);



            SwipeForQuestion1Video1.SetActive(false);
            SwipeForQuestion1Video2.SetActive(false);
            SwipeForQuestion2Video1.SetActive(false);
            SwipeForQuestion2Video2.SetActive(false);
            SwipeForQuestion3Video1.SetActive(false);
            SwipeForQuestion3Video2.SetActive(false);

            SwipeForQuestion4Video1.SetActive(false);
            SwipeForQuestion4Video2.SetActive(false);
            SwipeForQuestion5Video1.SetActive(false);
            SwipeForQuestion5Video2.SetActive(false);
            SwipeForQuestion6Video1.SetActive(false);
            SwipeForQuestion6Video2.SetActive(false);

            SwipeForQuestion7Video1.SetActive(false);
            SwipeForQuestion7Video2.SetActive(false);
            SwipeForQuestion8Video1.SetActive(false);
            SwipeForQuestion8Video2.SetActive(false);
            SwipeForQuestion9Video1.SetActive(false);
            SwipeForQuestion9Video2.SetActive(false);

            SwipeForQuestion10Video1.SetActive(false);
            SwipeForQuestion10Video2.SetActive(false);
            SwipeForQuestion11Video1.SetActive(false);
            SwipeForQuestion11Video2.SetActive(false);
            SwipeForQuestion12Video1.SetActive(false);
            SwipeForQuestion12Video2.SetActive(false);


            VideoToggle.Video1.SetActive(false);
            VideoToggle.Video2.SetActive(false);
            VideoToggle.Video3.SetActive(false);
            VideoToggle.Video4.SetActive(false);
            VideoToggle.Video5.SetActive(false);
            VideoToggle.Video6.SetActive(false);
            VideoToggle.Video7.SetActive(false);
            VideoToggle.Video8.SetActive(false);
            VideoToggle.Video9.SetActive(false);
            VideoToggle.Video10.SetActive(false);
            VideoToggle.Video11.SetActive(false);
            VideoToggle.Video12.SetActive(false);
            VideoToggle.Video1.SetActive(false);
            VideoToggle.Video14.SetActive(false);
            VideoToggle.Video15.SetActive(false);
            VideoToggle.Video16.SetActive(false);
            VideoToggle.Video17.SetActive(false);
            VideoToggle.Video18.SetActive(false);
            VideoToggle.Video19.SetActive(false);
            VideoToggle.Video20.SetActive(false);
            VideoToggle.Video21.SetActive(false);
            VideoToggle.Video22.SetActive(false);
            VideoToggle.Video23.SetActive(false);
            VideoToggle.Video24.SetActive(false);
            VideoToggle.IntroVideo.SetActive(true);

        }
    }

    public void DoThingsWhenGestureForAIForGoodQuestion3IsTriggered()
    {
        AIForGood.SetActive(true);
        if (AIForGood.activeSelf)
        {
            AIForYou.SetActive(false);
            AIForYourBusiness.SetActive(false);
            AIForYouAround.SetActive(false);

            MainMenu.SetActive(false);

            StartCoroutine(WaitForActivatingGestureForMainMenuAIForGood());

            GestureForMainMenuAIForYou.SetActive(false);
            GestureForMainMenuAIForGood.SetActive(false);
            GestureForMainMenuAIAroundYou.SetActive(false);

            GestureForAIForYouQuestion1.SetActive(false);
            GestureForAIForYouQuestion2.SetActive(false);
            GestureForAIForYouQuestion3.SetActive(false);

            GestureForAIForYourBusinessQuestion1.SetActive(false);
            GestureForAIForYourBusinessQuestion2.SetActive(false);
            GestureForAIForYourBusinessQuestion3.SetActive(false);

            GestureForAIForGoodQuestion1.SetActive(false);
            GestureForAIForGoodQuestion2.SetActive(false);
            GestureForAIForGoodQuestion3.SetActive(false);

            GestureForAIAroundYouQuestion1.SetActive(false);
            GestureForAIAroundYouQuestion2.SetActive(false);
            GestureForAIAroundYouQuestion3.SetActive(false);



            SwipeForQuestion1Video1.SetActive(false);
            SwipeForQuestion1Video2.SetActive(false);
            SwipeForQuestion2Video1.SetActive(false);
            SwipeForQuestion2Video2.SetActive(false);
            SwipeForQuestion3Video1.SetActive(false);
            SwipeForQuestion3Video2.SetActive(false);

            SwipeForQuestion4Video1.SetActive(false);
            SwipeForQuestion4Video2.SetActive(false);
            SwipeForQuestion5Video1.SetActive(false);
            SwipeForQuestion5Video2.SetActive(false);
            SwipeForQuestion6Video1.SetActive(false);
            SwipeForQuestion6Video2.SetActive(false);

            SwipeForQuestion7Video1.SetActive(false);
            SwipeForQuestion7Video2.SetActive(false);
            SwipeForQuestion8Video1.SetActive(false);
            SwipeForQuestion8Video2.SetActive(false);
            SwipeForQuestion9Video1.SetActive(false);
            SwipeForQuestion9Video2.SetActive(false);

            SwipeForQuestion10Video1.SetActive(false);
            SwipeForQuestion10Video2.SetActive(false);
            SwipeForQuestion11Video1.SetActive(false);
            SwipeForQuestion11Video2.SetActive(false);
            SwipeForQuestion12Video1.SetActive(false);
            SwipeForQuestion12Video2.SetActive(false);


            VideoToggle.Video1.SetActive(false);
            VideoToggle.Video2.SetActive(false);
            VideoToggle.Video3.SetActive(false);
            VideoToggle.Video4.SetActive(false);
            VideoToggle.Video5.SetActive(false);
            VideoToggle.Video6.SetActive(false);
            VideoToggle.Video7.SetActive(false);
            VideoToggle.Video8.SetActive(false);
            VideoToggle.Video9.SetActive(false);
            VideoToggle.Video10.SetActive(false);
            VideoToggle.Video11.SetActive(false);
            VideoToggle.Video12.SetActive(false);
            VideoToggle.Video1.SetActive(false);
            VideoToggle.Video14.SetActive(false);
            VideoToggle.Video15.SetActive(false);
            VideoToggle.Video16.SetActive(false);
            VideoToggle.Video17.SetActive(false);
            VideoToggle.Video18.SetActive(false);
            VideoToggle.Video19.SetActive(false);
            VideoToggle.Video20.SetActive(false);
            VideoToggle.Video21.SetActive(false);
            VideoToggle.Video22.SetActive(false);
            VideoToggle.Video23.SetActive(false);
            VideoToggle.Video24.SetActive(false);
            VideoToggle.IntroVideo.SetActive(true);

        }
    }

    public void DoThingsWhenGestureForAIAroundYouQuestion1IsTriggered()
    {
        AIForYouAround.SetActive(true);
        if (AIForYouAround.activeSelf)
        {
            AIForYou.SetActive(false);
            AIForYourBusiness.SetActive(false);
            AIForGood.SetActive(false);

            MainMenu.SetActive(false);

            StartCoroutine(WaitForActivatingGestureForMainMenuAIAroundYou());

            GestureForMainMenuAIForYou.SetActive(false);
            GestureForMainMenuAIForGood.SetActive(false);
            GestureForMainMenuAIAroundYou.SetActive(false);

            GestureForAIForYouQuestion1.SetActive(false);
            GestureForAIForYouQuestion2.SetActive(false);
            GestureForAIForYouQuestion3.SetActive(false);

            GestureForAIForYourBusinessQuestion1.SetActive(false);
            GestureForAIForYourBusinessQuestion2.SetActive(false);
            GestureForAIForYourBusinessQuestion3.SetActive(false);

            GestureForAIForGoodQuestion1.SetActive(false);
            GestureForAIForGoodQuestion2.SetActive(false);
            GestureForAIForGoodQuestion3.SetActive(false);

            GestureForAIAroundYouQuestion1.SetActive(false);
            GestureForAIAroundYouQuestion2.SetActive(false);
            GestureForAIAroundYouQuestion3.SetActive(false);



            SwipeForQuestion1Video1.SetActive(false);
            SwipeForQuestion1Video2.SetActive(false);
            SwipeForQuestion2Video1.SetActive(false);
            SwipeForQuestion2Video2.SetActive(false);
            SwipeForQuestion3Video1.SetActive(false);
            SwipeForQuestion3Video2.SetActive(false);

            SwipeForQuestion4Video1.SetActive(false);
            SwipeForQuestion4Video2.SetActive(false);
            SwipeForQuestion5Video1.SetActive(false);
            SwipeForQuestion5Video2.SetActive(false);
            SwipeForQuestion6Video1.SetActive(false);
            SwipeForQuestion6Video2.SetActive(false);

            SwipeForQuestion7Video1.SetActive(false);
            SwipeForQuestion7Video2.SetActive(false);
            SwipeForQuestion8Video1.SetActive(false);
            SwipeForQuestion8Video2.SetActive(false);
            SwipeForQuestion9Video1.SetActive(false);
            SwipeForQuestion9Video2.SetActive(false);

            SwipeForQuestion10Video1.SetActive(false);
            SwipeForQuestion10Video2.SetActive(false);
            SwipeForQuestion11Video1.SetActive(false);
            SwipeForQuestion11Video2.SetActive(false);
            SwipeForQuestion12Video1.SetActive(false);
            SwipeForQuestion12Video2.SetActive(false);


            VideoToggle.Video1.SetActive(false);
            VideoToggle.Video2.SetActive(false);
            VideoToggle.Video3.SetActive(false);
            VideoToggle.Video4.SetActive(false);
            VideoToggle.Video5.SetActive(false);
            VideoToggle.Video6.SetActive(false);
            VideoToggle.Video7.SetActive(false);
            VideoToggle.Video8.SetActive(false);
            VideoToggle.Video9.SetActive(false);
            VideoToggle.Video10.SetActive(false);
            VideoToggle.Video11.SetActive(false);
            VideoToggle.Video12.SetActive(false);
            VideoToggle.Video1.SetActive(false);
            VideoToggle.Video14.SetActive(false);
            VideoToggle.Video15.SetActive(false);
            VideoToggle.Video16.SetActive(false);
            VideoToggle.Video17.SetActive(false);
            VideoToggle.Video18.SetActive(false);
            VideoToggle.Video19.SetActive(false);
            VideoToggle.Video20.SetActive(false);
            VideoToggle.Video21.SetActive(false);
            VideoToggle.Video22.SetActive(false);
            VideoToggle.Video23.SetActive(false);
            VideoToggle.Video24.SetActive(false);
            VideoToggle.IntroVideo.SetActive(true);

        }
    }

    public void DoThingsWhenGestureForAIAroundYouQuestion2IsTriggered()
    {
        AIForYouAround.SetActive(true);
        if (AIForYouAround.activeSelf)
        {
            AIForYou.SetActive(false);
            AIForYourBusiness.SetActive(false);
            AIForGood.SetActive(false);

            MainMenu.SetActive(false);

            StartCoroutine(WaitForActivatingGestureForMainMenuAIAroundYou());

            GestureForMainMenuAIForYou.SetActive(false);
            GestureForMainMenuAIForGood.SetActive(false);
            GestureForMainMenuAIAroundYou.SetActive(false);

            GestureForAIForYouQuestion1.SetActive(false);
            GestureForAIForYouQuestion2.SetActive(false);
            GestureForAIForYouQuestion3.SetActive(false);

            GestureForAIForYourBusinessQuestion1.SetActive(false);
            GestureForAIForYourBusinessQuestion2.SetActive(false);
            GestureForAIForYourBusinessQuestion3.SetActive(false);

            GestureForAIForGoodQuestion1.SetActive(false);
            GestureForAIForGoodQuestion2.SetActive(false);
            GestureForAIForGoodQuestion3.SetActive(false);

            GestureForAIAroundYouQuestion1.SetActive(false);
            GestureForAIAroundYouQuestion2.SetActive(false);
            GestureForAIAroundYouQuestion3.SetActive(false);



            SwipeForQuestion1Video1.SetActive(false);
            SwipeForQuestion1Video2.SetActive(false);
            SwipeForQuestion2Video1.SetActive(false);
            SwipeForQuestion2Video2.SetActive(false);
            SwipeForQuestion3Video1.SetActive(false);
            SwipeForQuestion3Video2.SetActive(false);

            SwipeForQuestion4Video1.SetActive(false);
            SwipeForQuestion4Video2.SetActive(false);
            SwipeForQuestion5Video1.SetActive(false);
            SwipeForQuestion5Video2.SetActive(false);
            SwipeForQuestion6Video1.SetActive(false);
            SwipeForQuestion6Video2.SetActive(false);

            SwipeForQuestion7Video1.SetActive(false);
            SwipeForQuestion7Video2.SetActive(false);
            SwipeForQuestion8Video1.SetActive(false);
            SwipeForQuestion8Video2.SetActive(false);
            SwipeForQuestion9Video1.SetActive(false);
            SwipeForQuestion9Video2.SetActive(false);

            SwipeForQuestion10Video1.SetActive(false);
            SwipeForQuestion10Video2.SetActive(false);
            SwipeForQuestion11Video1.SetActive(false);
            SwipeForQuestion11Video2.SetActive(false);
            SwipeForQuestion12Video1.SetActive(false);
            SwipeForQuestion12Video2.SetActive(false);


            VideoToggle.Video1.SetActive(false);
            VideoToggle.Video2.SetActive(false);
            VideoToggle.Video3.SetActive(false);
            VideoToggle.Video4.SetActive(false);
            VideoToggle.Video5.SetActive(false);
            VideoToggle.Video6.SetActive(false);
            VideoToggle.Video7.SetActive(false);
            VideoToggle.Video8.SetActive(false);
            VideoToggle.Video9.SetActive(false);
            VideoToggle.Video10.SetActive(false);
            VideoToggle.Video11.SetActive(false);
            VideoToggle.Video12.SetActive(false);
            VideoToggle.Video1.SetActive(false);
            VideoToggle.Video14.SetActive(false);
            VideoToggle.Video15.SetActive(false);
            VideoToggle.Video16.SetActive(false);
            VideoToggle.Video17.SetActive(false);
            VideoToggle.Video18.SetActive(false);
            VideoToggle.Video19.SetActive(false);
            VideoToggle.Video20.SetActive(false);
            VideoToggle.Video21.SetActive(false);
            VideoToggle.Video22.SetActive(false);
            VideoToggle.Video23.SetActive(false);
            VideoToggle.Video24.SetActive(false);
            VideoToggle.IntroVideo.SetActive(true);

        }
    }

    public void DoThingsWhenGestureForAIAroundYouQuestion3IsTriggered()
    {
        AIForYouAround.SetActive(true);
        if (AIForYouAround.activeSelf)
        {
            AIForYou.SetActive(false);
            AIForYourBusiness.SetActive(false);
            AIForGood.SetActive(false);

            MainMenu.SetActive(false);

            StartCoroutine(WaitForActivatingGestureForMainMenuAIAroundYou());

            GestureForMainMenuAIForYou.SetActive(false);
            GestureForMainMenuAIForGood.SetActive(false);
            GestureForMainMenuAIAroundYou.SetActive(false);

            GestureForAIForYouQuestion1.SetActive(false);
            GestureForAIForYouQuestion2.SetActive(false);
            GestureForAIForYouQuestion3.SetActive(false);

            GestureForAIForYourBusinessQuestion1.SetActive(false);
            GestureForAIForYourBusinessQuestion2.SetActive(false);
            GestureForAIForYourBusinessQuestion3.SetActive(false);

            GestureForAIForGoodQuestion1.SetActive(false);
            GestureForAIForGoodQuestion2.SetActive(false);
            GestureForAIForGoodQuestion3.SetActive(false);

            GestureForAIAroundYouQuestion1.SetActive(false);
            GestureForAIAroundYouQuestion2.SetActive(false);
            GestureForAIAroundYouQuestion3.SetActive(false);



            SwipeForQuestion1Video1.SetActive(false);
            SwipeForQuestion1Video2.SetActive(false);
            SwipeForQuestion2Video1.SetActive(false);
            SwipeForQuestion2Video2.SetActive(false);
            SwipeForQuestion3Video1.SetActive(false);
            SwipeForQuestion3Video2.SetActive(false);

            SwipeForQuestion4Video1.SetActive(false);
            SwipeForQuestion4Video2.SetActive(false);
            SwipeForQuestion5Video1.SetActive(false);
            SwipeForQuestion5Video2.SetActive(false);
            SwipeForQuestion6Video1.SetActive(false);
            SwipeForQuestion6Video2.SetActive(false);

            SwipeForQuestion7Video1.SetActive(false);
            SwipeForQuestion7Video2.SetActive(false);
            SwipeForQuestion8Video1.SetActive(false);
            SwipeForQuestion8Video2.SetActive(false);
            SwipeForQuestion9Video1.SetActive(false);
            SwipeForQuestion9Video2.SetActive(false);

            SwipeForQuestion10Video1.SetActive(false);
            SwipeForQuestion10Video2.SetActive(false);
            SwipeForQuestion11Video1.SetActive(false);
            SwipeForQuestion11Video2.SetActive(false);
            SwipeForQuestion12Video1.SetActive(false);
            SwipeForQuestion12Video2.SetActive(false);


            VideoToggle.Video1.SetActive(false);
            VideoToggle.Video2.SetActive(false);
            VideoToggle.Video3.SetActive(false);
            VideoToggle.Video4.SetActive(false);
            VideoToggle.Video5.SetActive(false);
            VideoToggle.Video6.SetActive(false);
            VideoToggle.Video7.SetActive(false);
            VideoToggle.Video8.SetActive(false);
            VideoToggle.Video9.SetActive(false);
            VideoToggle.Video10.SetActive(false);
            VideoToggle.Video11.SetActive(false);
            VideoToggle.Video12.SetActive(false);
            VideoToggle.Video1.SetActive(false);
            VideoToggle.Video14.SetActive(false);
            VideoToggle.Video15.SetActive(false);
            VideoToggle.Video16.SetActive(false);
            VideoToggle.Video17.SetActive(false);
            VideoToggle.Video18.SetActive(false);
            VideoToggle.Video19.SetActive(false);
            VideoToggle.Video20.SetActive(false);
            VideoToggle.Video21.SetActive(false);
            VideoToggle.Video22.SetActive(false);
            VideoToggle.Video23.SetActive(false);
            VideoToggle.Video24.SetActive(false);
            VideoToggle.IntroVideo.SetActive(true);

        }
    }

    IEnumerator WaitForActivatingGestureForMainMenuAIForYou()
    {
        yield return new WaitForSeconds(3.0f);
        GestureForMainMenuAIForYou.SetActive(true);
    }

    IEnumerator WaitForActivatingGestureForMainMenuAIForYourBusiness()
    {
        yield return new WaitForSeconds(3.0f);
        GestureForMainMenuAIForYourBusiness.SetActive(true);
    }

    IEnumerator WaitForActivatingGestureForMainMenuAIForGood()
    {
        yield return new WaitForSeconds(3.0f);
        GestureForMainMenuAIForGood.SetActive(true);
    }

    IEnumerator WaitForActivatingGestureForMainMenuAIAroundYou()
    {
        yield return new WaitForSeconds(3.0f);
        GestureForMainMenuAIAroundYou.SetActive(true);
    }

    public void DOThingsWhenGestureForQuestion1Video1IsTriggered()
    {
        SwipeForQuestion1Video1.SetActive(false);
        StartCoroutine(DelaySwipeForQuestion1Video2());


    }
    IEnumerator DelaySwipeForQuestion1Video2()
    {
        yield return new WaitForSeconds(10.0f);
        SwipeForQuestion1Video2.SetActive(true);
    }

    public void DOThingsWhenGestureForQuestion1Video2IsTriggered()
    {

    }

    public void DOThingsWhenGestureForQuestion2Video1IsTriggered()
    {
        StartCoroutine(DelaySwipeForQuestion2Video2());
        SwipeForQuestion2Video1.SetActive(false);
    }
    IEnumerator DelaySwipeForQuestion2Video2()
    {
        yield return new WaitForSeconds(10.0f);
        SwipeForQuestion2Video2.SetActive(true);
    }
    public void DOThingsWhenGestureForQuestion2Video2IsTriggered()
    {

    }
    public void DOThingsWhenGestureForQuestion3Video1IsTriggered()
    {
        StartCoroutine(DelaySwipeForQuestion3Video2());
        SwipeForQuestion3Video1.SetActive(false);
    }
    IEnumerator DelaySwipeForQuestion3Video2()
    {
        yield return new WaitForSeconds(10.0f);
        SwipeForQuestion3Video2.SetActive(true);
    }
    public void DOThingsWhenGestureForQuestion3Video2IsTriggered()
    {

    }
    public void DOThingsWhenGestureForQuestion4Video1IsTriggered()
    {
        StartCoroutine(DelaySwipeForQuestion4Video2());
        SwipeForQuestion4Video1.SetActive(false);
    }
    IEnumerator DelaySwipeForQuestion4Video2()
    {
        yield return new WaitForSeconds(10.0f);
        SwipeForQuestion4Video2.SetActive(true);
    }
    public void DOThingsWhenGestureForQuestion4Video2IsTriggered()
    {

    }
    public void DOThingsWhenGestureForQuestion5Video1IsTriggered()
    {
        StartCoroutine(DelaySwipeForQuestion5Video2());
        SwipeForQuestion5Video1.SetActive(false);
    }
    IEnumerator DelaySwipeForQuestion5Video2()
    {
        yield return new WaitForSeconds(10.0f);
        SwipeForQuestion5Video2.SetActive(true);
    }
    public void DOThingsWhenGestureForQuestion5Video2IsTriggered()
    {

    }

    public void DOThingsWhenGestureForQuestion6Video1IsTriggered()
    {
        StartCoroutine(DelaySwipeForQuestion6Video2());
        SwipeForQuestion6Video1.SetActive(false);
    }
    IEnumerator DelaySwipeForQuestion6Video2()
    {
        yield return new WaitForSeconds(10.0f);
        SwipeForQuestion6Video2.SetActive(true);
    }


    public void DOThingsWhenGestureForQuestion6Video2IsTriggered()
    {

    }

    public void DOThingsWhenGestureForQuestion7Video1IsTriggered()
    {
        StartCoroutine(DelaySwipeForQuestion7Video2());
        SwipeForQuestion6Video1.SetActive(false);
    }

    IEnumerator DelaySwipeForQuestion7Video2()
    {
        yield return new WaitForSeconds(10.0f);
        SwipeForQuestion7Video2.SetActive(true);
    }

    public void DOThingsWhenGestureForQuestion7Video2IsTriggered()
    {

    }

    public void DOThingsWhenGestureForQuestion8Video1IsTriggered()
    {
        StartCoroutine(DelaySwipeForQuestion8Video2());
        SwipeForQuestion8Video1.SetActive(false);
    }
    IEnumerator DelaySwipeForQuestion8Video2()
    {
        yield return new WaitForSeconds(10.0f);
        SwipeForQuestion8Video2.SetActive(true);
    }


    public void DOThingsWhenGestureForQuestion8Video2IsTriggered()
    {

    }

    public void DOThingsWhenGestureForQuestion9Video1IsTriggered()
    {
        StartCoroutine(DelaySwipeForQuestion9Video2());
        SwipeForQuestion9Video1.SetActive(false);
    }

    IEnumerator DelaySwipeForQuestion9Video2()
    {
        yield return new WaitForSeconds(10.0f);
        SwipeForQuestion9Video2.SetActive(true);
    }

    public void DOThingsWhenGestureForQuestion9Video2IsTriggered()
    {

    }

    public void DOThingsWhenGestureForQuestion10Video1IsTriggered()
    {
        StartCoroutine(DelaySwipeForQuestion10Video2());
        SwipeForQuestion10Video1.SetActive(false);
    }
    IEnumerator DelaySwipeForQuestion10Video2()
    {
        yield return new WaitForSeconds(10.0f);
        SwipeForQuestion10Video2.SetActive(true);
    }

    public void DOThingsWhenGestureForQuestion10Video2IsTriggered()
    {

    }

    public void DOThingsWhenGestureForQuestion11Video1IsTriggered()
    {
        StartCoroutine(DelaySwipeForQuestion11Video2());
        SwipeForQuestion11Video1.SetActive(false);
    }
    IEnumerator DelaySwipeForQuestion11Video2()
    {
        yield return new WaitForSeconds(10.0f);
        SwipeForQuestion11Video2.SetActive(true);
    }

    public void DOThingsWhenGestureForQuestio11Video2IsTriggered()
    {

    }

    public void DOThingsWhenGestureForQuestion12Video1IsTriggered()
    {
        StartCoroutine(DelaySwipeForQuestion12Video2());
        SwipeForQuestion12Video1.SetActive(false);
    }

    IEnumerator DelaySwipeForQuestion12Video2()
    {
        yield return new WaitForSeconds(10.0f);
        SwipeForQuestion12Video2.SetActive(true);
    }

    public void DOThingsWhenGestureForQuestion12Video2IsTriggered()
    {

    }


    IEnumerator SwipeVideo1AIForYourBusiness()
    {

        yield return new WaitForSeconds(2.0f);
        SwipeForQuestion4Video2.SetActive(true);
        SwipeForQuestion4Video1.SetActive(false);
    }
}
