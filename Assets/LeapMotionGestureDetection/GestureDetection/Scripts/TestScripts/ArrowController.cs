﻿using UnityEngine;
using UnityEngine.Video;

public class ArrowController : MonoBehaviour
{
    public void Show() {
        // GetComponent<VideoPlayer>().Play();
        GetComponent<SpriteRenderer>().enabled = true;
    }
    public void Hide() {
        // GetComponent<VideoPlayer>().Stop();
        GetComponent<SpriteRenderer>().enabled = false;
    }
}