﻿using UnityEngine;
using UnityEngine.SceneManagement;
public class TestingTransition : MonoBehaviour
{

    public Animator animator;

    private int levelToLoad;

    // Update is called once per frame
    void Update()
    {
        
    }

    public void FadeToLevel(int levelIndex)
    {
        levelToLoad = levelIndex;
        animator.SetTrigger("FadeOut");
    }

    public void Transition()
    {
        FadeToLevel(2);
    }

    public void OnFadeComplete()
    {
        SceneManager.LoadScene(levelToLoad, LoadSceneMode.Additive);
    }
}
