﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestSelectableObject : GestureBase
{

    public EHand m_Hand;

    [Range(0.0f, 1.0f)]
    public float m_ClosedPercentage = 0.6f;

    bool m_bDetected = false;

    public void StartActivation()
    {

        if (DetectionManager.Get().IsHandSet(m_Hand))
        {
            if (DetectionManager.Get().GetHand(m_Hand).IsClosed(m_ClosedPercentage))
            {
                m_bDetected = true;
            }
        }
        
    }

    public void EndActivation()
    {
        m_bDetected = false;
    }

    public override bool Detected()
    {
        return m_bDetected;
    }

}
