﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowThreeOptions : MonoBehaviour
{
    public GameObject QuestionOption1;
    public GameObject QuestionOption2;
    public GameObject QuestionOption3;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void EnableThreeOptions()
    {
        QuestionOption1.SetActive(true);
        QuestionOption2.SetActive(true);
        QuestionOption3.SetActive(true);
    }
}
