﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeactivateOptions : MonoBehaviour
{
    public GameObject Option1Question1;
    public GameObject Option2Question1;
    public GameObject Option1Question2;
    public GameObject Option2Question2;
    public GameObject Option3Question2;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DeactivateOptionsOFQuestions()
    {
        Option1Question1.SetActive(false);
        Option2Question1.SetActive(false);
        Option1Question2.SetActive(false);
        Option2Question2.SetActive(false);
        Option3Question2.SetActive(false);
    }
}
