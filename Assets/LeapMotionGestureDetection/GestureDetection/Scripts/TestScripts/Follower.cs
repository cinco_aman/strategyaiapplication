﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Follower : MonoBehaviour
{    
    private bool isFollowing;
    private bool isReturning;
    private IEnumerator followRoutine;
    private IEnumerator returnRoutine;
    private Vector3 homePosition;
    private Quaternion homeRotation;
    
    #region UnityEvents

    void Start()
    {
        isFollowing = false;
        isReturning = false;
        followRoutine = null;
        returnRoutine = null;
        homePosition = transform.position;
        homeRotation = transform.rotation;
        
    }
    
    private void OnDestroy()
    {
        this.StopAllCoroutines();
    }
    
    #endregion
    
    #region Private Methods
    private Vector3 GetPosition(EHand mHand)
    {
        return DetectionManager.Get().GetHand(mHand).GetHandPosition();
    }
    
    private IEnumerator FollowCoroutine(EHand mHand,Vector3 offset)
    {
        isFollowing = true;

        while (true)
        {
            Vector3 newPos = GetPosition(mHand) + offset;

            transform.position = new Vector3(newPos.x,newPos.y, transform.position.z);
            yield return new WaitForEndOfFrame();
        }
        
        isFollowing = false;
    }
    
    private IEnumerator ReturnToHomeCoroutine()
    {
        Vector3 currentPosition = transform.position;
        Quaternion currentRotation = transform.rotation;

        float x = 0;
        isReturning = true;

        while (x < 1.0f) {
            transform.position = Vector3.Lerp(currentPosition, homePosition, x);
            transform.rotation = Quaternion.Lerp(currentRotation, homeRotation, x);
            x += Time.deltaTime * 1.0f;
            yield return new WaitForEndOfFrame();
        }
        
        isReturning = false;
    }

    #endregion

    #region Public Methods

    public void StartFollow(EHand mHand)
    {
        if (isFollowing)
        {
            Debug.LogWarning("Trying to follow but it's already following. Please StopFollow() before call it a again. Skipping.");
            return;
        }

        Vector3 offset = transform.position - GetPosition(mHand);
        followRoutine = FollowCoroutine(mHand,offset);
        StartCoroutine(followRoutine);
    }
    
    public void StopFollow()
    {
        if (!isFollowing)
        {
            Debug.LogWarning("Trying to stop following but is not following. Skipping");
            return;
        }

        StopCoroutine(followRoutine);
        isFollowing = false;
    }

    public void StartReturn()
    {
        if (isReturning)
        {
            Debug.LogWarning("Object is already returning. Skipping");
            return;
        }

        returnRoutine = ReturnToHomeCoroutine();
        StartCoroutine(returnRoutine);
    }
    
    public void StopReturn()
    {
        if (!isReturning)
        {
            Debug.LogWarning("Object is not returning. Skipping");
            return;
        }
        
        StopCoroutine(returnRoutine);
        isReturning = false;
    }

    public bool IsFollowing () => isFollowing;

    public bool IsReturning () => isReturning;

    #endregion
}