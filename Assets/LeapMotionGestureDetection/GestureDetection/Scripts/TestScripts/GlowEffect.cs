﻿using UnityEngine;

public class GlowEffect : MonoBehaviour
{
    [SerializeField] private GameObject effectHolder;

    private bool isActive;


    public void On()
    {
        var glows = GameObject.FindObjectsOfType<GlowEffect>();

        foreach (var glow in glows) {
            if (glow.isActive)
                return;
        }

        effectHolder.SetActive(true);
        isActive = true;
    }
    public void CleanUp() {
        effectHolder.SetActive(false);
        isActive = false;
    }
    public void Off()
    {
        var followers = GameObject.FindObjectsOfType<Follower>();

        foreach (var follower in followers) {
            if (follower.IsFollowing()) {
                return;
            }
        }

        effectHolder.SetActive(false);
        isActive = false;
    }

    public bool IsActive() => isActive;

    public GameObject GetHolder() => effectHolder;

    public void SetHolder(GameObject holder)
    {
        effectHolder = holder;
    }
}