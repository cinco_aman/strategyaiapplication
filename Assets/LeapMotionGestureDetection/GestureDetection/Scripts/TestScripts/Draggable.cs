﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
[RequireComponent(typeof(FistGesture))]
public class Draggable : MonoBehaviour
{
    private Follower mFollower;
    private EHand mHand;
    private HandDetector mHandDetector;

    [System.Serializable] public class GameObjectEvent : UnityEvent<GameObject> { }

    public GameObjectEvent OnDragStart;
    public GameObjectEvent OnDragEnd;
    public GameObjectEvent OnDragStay;
    public GameObjectEvent OnHoverStart;
    public GameObjectEvent OnHoverEnd;
    
    void Awake()
    {
        mFollower = GetComponent<Follower>();
        mHand = GetComponent<FistGesture>().m_Hand;
        mHandDetector = gameObject.AddComponent<HandDetector>();

        mHandDetector.SetHand(mHand);
    }

    private void OnTriggerEnter(Collider other)
    {
        switch (other.gameObject.tag)
        {
            case "AIToday":
            case "AITomorrow":
            case "AIGood":
            case "AIAroundYou":
                return;
        }
        
        OnHoverStart.Invoke(gameObject);
    }

    private void Update()
    {
        if (mFollower.IsFollowing()) {
            OnDragStay.Invoke(gameObject);
        }
    }

    public void CleanUp() {
        OnDragEnd.Invoke(gameObject);
        OnHoverEnd.Invoke(gameObject);
        mFollower.StopAllCoroutines();
    }

    private void OnTriggerExit(Collider other)
    {
        switch (other.gameObject.tag)
        {
            case "AIToday":
            case "AITomorrow":
            case "AIGood":
            case "AIAroundYou":
                return;
        }

        OnHoverEnd.Invoke(gameObject);
    }

    public void StartDrag()
    {
        if (!mHandDetector.IsOnHand())
        {
            return;
        }
        
        var followers = GameObject.FindObjectsOfType<Follower>();

        foreach (Follower follower in followers)
        {
            if (follower.IsFollowing())
            {
                return;
            }
        }

        var scaleAnimations = GameObject.FindObjectsOfType<ScaleAnimation>();
        
        foreach(var scaleAnim in scaleAnimations) {
            if (scaleAnim.IsMoving() && scaleAnim.IsScaling())
            {
                return;
            }
        }

        CapsuleCollider[] colliders = GameObject.FindObjectsOfType<CapsuleCollider>();
        CapsuleCollider mCollider = GetComponent<CapsuleCollider>();

        foreach(var col in colliders)
        {
            if (col.isTrigger && col != mCollider) {
                col.enabled = false;
            }
        }
        
        if (mFollower.IsReturning())
        {
             mFollower.StopReturn();
             mFollower.StartFollow(mHand);
             OnDragStart.Invoke(gameObject);
             return;
        }
        
        mFollower.StartFollow(mHand);
        OnDragStart.Invoke(gameObject);
        
    }
    public void StopDrag()
    {
        if (mFollower.IsFollowing())
        {
            mFollower.StopFollow();
            mFollower.StartReturn();
            OnDragEnd.Invoke(gameObject);
            CapsuleCollider[] colliders = GameObject.FindObjectsOfType<CapsuleCollider>();

            foreach (var col in colliders)
            {
                col.enabled = true;
            }
        }
    }
}