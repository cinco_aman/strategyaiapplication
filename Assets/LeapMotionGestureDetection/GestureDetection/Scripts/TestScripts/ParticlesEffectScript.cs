﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ParticlesEffectScript : MonoBehaviour
{
    public void ActivateEffect1()
    {
        
        StartCoroutine(LoadSceneAfterAnimation());
       
    }
    IEnumerator LoadSceneAfterAnimation()
    {
        yield return new WaitForSeconds(4.0f);
        SceneManager.LoadScene("NewOrbSceneFinal");
    }
}
